<?php 

/**
*
*/

class Services_inscripcionesController extends Controllers_Abstract
{

	public function indexAction()
	{
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *'); 
		$data = $this->getData();
		$inscripcionesModel = new Page_Model_DbTable_Inscripciones();
		$id = $inscripcionesModel->insert($data); 
		$inscripcionesModel->changeOrder($id,$id);
		$email = new Core_Model_Sendingemail($this->_view); 
		$res = $email->confirmacion2($data);
		echo $respuesta = json_encode(array('respuesta' =>$res));
	}
	private function getData()
	{
		$data = array();
		$data['inscripciones_nombre'] = $this->_getSanitizedParam("inscripciones_nombre");
		$data['inscripcion_documento'] = $this->_getSanitizedParam("inscripcion_documento");
		$data['inscripciones_correo'] = $this->_getSanitizedParam("inscripciones_correo");
		$data['inscripciones_telefono'] = $this->_getSanitizedParam("inscripciones_telefono");
		$data['inscripciones_evento'] = $this->_getSanitizedParam("inscripciones_evento");
		if($this->_getSanitizedParam("inscripcion_estado") == '' ) {
			$data['inscripcion_estado'] = '0';
		} else {
			$data['inscripcion_estado'] = $this->_getSanitizedParam("inscripcion_estado"); 
		}
		$data['inscripcion_fecha'] = date('Y-m-d');
		return $data;
	}
}