<?php 

/**
*
*/

class Services_jeunetController extends Controllers_Abstract
{
    public function indexAction(){
        header('Access-Control-Allow-Origin: *');  
        $contenidoModel = new Services_Model_DbTable_Contenido();
        $this->_view->jeunet = $contenidoModel->getList("contenido_seccion = '9'","contenido_fecha DESC");
        header('Content-Type: application/json');
    }
     public function detalleAction(){
        header('Access-Control-Allow-Origin: *'); 
        header('Content-Type: application/json');
        $contenidoModel = new Services_Model_DbTable_Contenido();
        $identificador = $this->_getSanitizedParam("id");
        $this->_view->jeunetdetalle = $contenidoModel->getById($identificador);
    }
} 