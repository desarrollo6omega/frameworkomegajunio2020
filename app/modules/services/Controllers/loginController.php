<?php 

class Services_loginController extends Page_mainController{

	public function indexAction()
	{

		Session::getInstance()->set("error_login","");
		$isPost = $this->getRequest()->isPost();
		$user= $this->_getSanitizedParam("usuario");
		$password = $this->_getSanitizedParam("password");
		$isError = false;
		$busco = "no";
		$error = 0;
		if($user && $password){
			$userModel = new core_Model_DbTable_User();
			$busco = "si";
			if ($userModel->autenticateUser($user,$password) == true) {
				$resUser = $userModel->searchUserByUser($user);
				$this->setLayout('blanco');
	            
				if($resUser->user_state == 1){
					$data = $resUser;
					$datainsert = [''];
		            $datainsert['ingresos_usuario'] = $resUser->user_id; 
		            $datainsert['ingresos_fecha'] = date("Y-m-d");
		            $datainsert['ingresos_desde'] = 1;
		            $ingresosModel = new Services_Model_DbTable_Ingresos(); 
					$ingresosModel->insert($datainsert);
				} else { 
					$isError = true;
					$error = 3;
					$mensaje = "El Usuario se encuentra inactivo.";
				}
			} else {
				$isError = true;
				$mensaje = "El Usuario o Contraseña son incorrectos.";
			}
		} else {
			$isError = true;
			$error=1;
			$mensaje = "Lo sentimos, ocurrio un error intente de nuevo.";
		}
		$json = array('error' =>$isError ,'mensaje' =>$mensaje, 'user' => $data );
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *'); 
		header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
		header('Accept','application/json');
		$this->_view->array = json_encode($json); 
		$this->setLayout('blanco');
	}
	public function forgotpassword2Action()
	{
		$this->setLayout('blanco');
        $this->_csrf_section = "login_admin";
        $modelUser = new Core_Model_DbTable_User();
        $email = $this->_getSanitizedParam("email");
        $error = true;
        $mensaje = "Usuario No encontrado";
        $filter = " user_email = '".$email."' ";
        $user = $modelUser->getList($filter, "")[0];
        $id = $user->user_id; 
        Session::getInstance()->set("error_olvido",$mensaje);
        if ($user) {
         	$sendingemail = new Core_Model_Sendingemail($this->_view); 
            $code = Session::getInstance()->get('csrf')['page_csrf'];
            $modelUser->editCode($id,$code);
            $user = $modelUser->getById($user->user_id);
            if ($sendingemail->forgotpassword2($user) == 1 ) { 
                $error = 1;
                $mensaje = "Se ha enviado a su correo un mensaje de recuperación de contraseña.";
                Session::getInstance()->set("mensaje_olvido",$mensaje);
                Session::getInstance()->set("error_olvido","");
            } else {
                $mensaje = "Lo sentimos ocurrio un error y no se pudo enviar su mensaje";
                Session::getInstance()->set("error_olvido",$mensaje);
            }
        }
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *'); 
		header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
		header('Accept','application/json');
		$json = array('error' =>$error ,'mensaje' =>$mensaje, 'email' => $email );
		echo json_encode($json);
	}

	public function logoutAction()
	{
		//LOG
		$data['log_tipo'] = "LOGOUT";
		$logModel = new Administracion_Model_DbTable_Log();
		$logModel->insert($data);

		Session::getInstance()->set("kt_login_id","");
		Session::getInstance()->set("kt_login_level","");
		Session::getInstance()->set("kt_login_user","");
		Session::getInstance()->set("kt_login_name","");
		header('Location: /page/');
	}
}
?>