<?php 

/**
*
*/

class Services_formatosController extends Controllers_Abstract
{

	public function indexAction()
	{
		header('Access-Control-Allow-Origin: *'); 
		header('Content-Type: application/json');
        $formatoModel = new Page_Model_DbTable_Formatos();
        $this->_view->formato = $formatoModel->getList("", "orden ASC");
	}

}