<?php 

/**
*
*/

class Services_eventosController extends Controllers_Abstract
{

	public function indexAction()
	{
		header('Access-Control-Allow-Origin: *'); 
		$albumModel = new Services_Model_DbTable_Album();
		$this->_view->album = $albumModel->getList("","orden ASC");
		header('Content-Type: application/json');
	}
	public function galeriaindexAction()
	{
		header('Access-Control-Allow-Origin: *'); 
		$albumModel = new Services_Model_DbTable_Album();
		$this->_view->albumindex = $albumModel->getListPages("","album_fecha DESC",0,2);
		header('Content-Type: application/json');
	}
	public function detalleAction(){
		header('Access-Control-Allow-Origin: *'); 
        header('Content-Type: application/json');
        $contenidoModel = new Services_Model_DbTable_Fotos();
        $identificador = $this->_getSanitizedParam("id");
       	$this->_view->fotos = $contenidoModel->getList("fotos_album = $identificador", "orden ASC"); 
	}
	public function categoriagaleria(){
		$contenidoModel = new Services_Model_DbTable_Fotos();
		$identificador = $this->_getSanitizedParam("id");
		$this->_view->fotos = $contenidoModel->getList("fotos_album = $identificador", "orden ASC"); 
	}

}