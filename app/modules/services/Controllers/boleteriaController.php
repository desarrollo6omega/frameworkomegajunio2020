<?php 

/**
*
*/

class Services_boleteriaController extends Controllers_Abstract
{

	public function indexAction()
	{
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *'); 
		$data = $this->getData();
		$inscripcionesModel = new Page_Model_DbTable_Inscripcionesboleteria();
		$id = $inscripcionesModel->insert($data);
		$inscripcionesModel->changeOrder($id,$id);
		$email = new Core_Model_Sendingemail($this->_view); 
		$res = $email->confirmacion3($data);
		echo $respuesta = json_encode(array('respuesta' =>$res));
	}
	private function getData()
	{
		$data = array();
		if($this->_getSanitizedParam("inscripcionboleteria_estado") == '' ) {
			$data['inscripcionboleteria_estado'] = '0';
		} else {
			$data['inscripcionboleteria_estado'] = $this->_getSanitizedParam("inscripcionboleteria_estado");
		}
		$data['inscripcionboleteria_documento'] = $this->_getSanitizedParam("inscripcionboleteria_documento");
		$data['inscripcionboleteria_nombre'] = $this->_getSanitizedParam("inscripcionboleteria_nombre");
		$data['inscripcionboleteria_correo'] = $this->_getSanitizedParam("inscripcionboleteria_correo");
		$data['inscripcionboleteria_telefono'] = $this->_getSanitizedParam("inscripcionboleteria_telefono");
		$data['inscripcionboleteria_direccion'] = $this->_getSanitizedParam("inscripcionboleteria_direccion");
		$data['inscripcionboleteria_evento'] = $this->_getSanitizedParam("inscripcionboleteria_evento");
		$data['inscripcionboleteria_tipoboleta'] = $this->_getSanitizedParam("inscripcionboleteria_tipoboleta");
		$data['inscripcionboleteria_cantidad'] = $this->_getSanitizedParam("inscripcionboleteria_cantidad");
		$data['inscripcionboleteria_total'] = $this->_getSanitizedParam("inscripcionboleteria_total");
		$data['inscripcionboleteria_fecha'] = date('Y-m-d');
		return $data;
    }
}