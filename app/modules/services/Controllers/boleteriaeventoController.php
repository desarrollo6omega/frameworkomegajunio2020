<?php 

/**
*
*/

class Services_boleteriaeventoController extends Controllers_Abstract
{

	public function indexAction()
	{
		header('Access-Control-Allow-Origin: *'); 
		$fechaactual = date('Y-m-d');
		$eventosModel = new Services_Model_DbTable_Boleteriaevento();
		$this->_view->boleteriaevento = $eventosModel->getList("'$fechaactual' < boleteriaevento_fechalimite","orden ASC");
		header('Content-Type: application/json');
	}
}