<?php 

/**
*
*/

class Services_categoriasController extends Controllers_Abstract
{
 
	public function indexAction()
	{
		header('Access-Control-Allow-Origin: *'); 
		header('Content-Type: application/json');
		$categoriasModel = new Services_Model_DbTable_Categorias();
		$this->_view->categorias = $categoriasModel->getList("","orden ASC");
	}
}