<?php 

/**
*
*/

class Services_boletaeventoController extends Controllers_Abstract
{

	public function indexAction()
	{
		header('Access-Control-Allow-Origin: *'); 
		$fechaactual = date('Y-m-d');
		$eventosModel = new Services_Model_DbTable_Boletaevento();
		$id = $this->_getSanitizedParam("id");
		$this->_view->boletaevento = $eventosModel->getList("boletaevento_evento = '$id'","orden ASC");
		header('Content-Type: application/json');
	}
	
	public function boletaAction(){
	 	header('Access-Control-Allow-Origin: *'); 
        header('Content-Type: application/json');
        $id = $this->_getSanitizedParam("id");
        $boletaModel = new Services_Model_DbTable_Boletaevento(); 
        $this->_view->boletas = $boletaModel->getList("boletaevento_evento = '$id'","");
	}
}