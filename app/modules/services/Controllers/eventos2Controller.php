<?php 

/**
*
*/

class Services_eventos2Controller extends Controllers_Abstract
{

	public function indexAction()
	{
		header('Access-Control-Allow-Origin: *'); 
		$fechaactual = date('Y-m-d');
		$eventosModel = new Services_Model_DbTable_Eventos();
		$this->_view->eventos2 = $eventosModel->getList("'$fechaactual' < evento_fechafinal AND evento_estado = '1'","orden ASC");
		header('Content-Type: application/json');
	}
}