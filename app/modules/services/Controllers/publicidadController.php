<?php 

/**
*
*/

class Services_publicidadController extends Controllers_Abstract
{
    public function indexAction(){
        header('Access-Control-Allow-Origin: *');  
        $publicidadModel = new Services_Model_DbTable_Publicidad();
        $this->_view->publicidad = $publicidadModel->getList("publicidad_seccion = '1' AND publicidad_estado = '1'","orden ASC");
        header('Content-Type: application/json'); 
    }
}