<?php 

/**
*
*/

class Services_contactoController extends Controllers_Abstract
{ 
    public function enviarAction()
	{
		$this->setLayout('blanco');
		$data = [''];
		$data ['nombre'] = $this->_getSanitizedParam('nombre'); 
		$data ['email'] = $this->_getSanitizedParam('email');
        $data ['telefono'] = $this->_getSanitizedParam('telefono');
        $data ['ciudad'] = $this->_getSanitizedParam('ciudad');
		$data ['mensaje'] = $this->_getSanitizedParam('mensaje');
		$email = new Core_Model_Sendingemail($this->_view); 
		echo $res = $email->enviarcorreo($data);
		header('Access-Control-Allow-Origin: *'); 
		header('Content-Type: application/json');
	}	
}