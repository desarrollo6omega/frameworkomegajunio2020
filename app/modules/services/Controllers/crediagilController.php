<?php 

/**
*
*/

class Services_crediagilController extends Controllers_Abstract
{

	public function indexAction()
	{
	header('Content-Type: application/json');
	header('Access-Control-Allow-Origin: *');
        $data = [''];
        $data ['fecha'] = $this->_getSanitizedParam('fecha'); 
        $data ['monto'] = $this->_getSanitizedParam('monto');
        $data ['extension'] = $this->_getSanitizedParam('extension');
        $data ['plazo'] = $this->_getSanitizedParam('plazo');
        $data ['nombre1'] = $this->_getSanitizedParam('nombre1'); 
        $data ['ubicacion'] = $this->_getSanitizedParam('ubicacion');
        $data ['codigonomina'] = $this->_getSanitizedParam('codigonomina');
        $data ['nombre2'] = $this->_getSanitizedParam('nombre2');
        $data ['cuenta'] = $this->_getSanitizedParam('cuenta');
        $data ['cedula'] = $this->_getSanitizedParam('cedula');
        $data ['acepto'] = $this->_getSanitizedParam('acepto');
        $data ['noacepto'] = $this->_getSanitizedParam('noacepto');
        $crediagilModel = new Page_Model_DbTable_Crediagil();
        $crediagil = $crediagilModel->insert($data); 
        $email = new Core_Model_Sendingemail($this->_view); 
        $res = $email->enviarcorreo2($data, $crediagil);
        echo $respuesta = json_encode(array('respuesta' =>$res));
	}
}