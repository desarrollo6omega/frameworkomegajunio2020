<?php 

/**
*
*/

class Services_conveniosController extends Controllers_Abstract
{

    public function indexAction()
    {
        header('Access-Control-Allow-Origin: *');
        $conveniosModel = new Services_Model_DbTable_Convenios();
        $convenioscategoriaModel = new Services_Model_DbTable_Convenioscategoria();
        $categorias = $convenioscategoriaModel->getList("", "orden ASC");
        $array = array();
        foreach ($categorias as $key => $categoria) {
            $identificador = $categoria->convenios_categoria_id;
            $array[$key] = [];
            $array[$key]['detalle'] = $categoria;
            $array[$key]['convenios'] = $conveniosModel->getList("convenios_categoria = '$identificador'", "orden ASC");
        }
        header('Content-Type: application/json');
        $this->_view->categorias = $array;
    }
    public function detalleAction(){
        header('Access-Control-Allow-Origin: *'); 
        header('Content-Type: application/json');
        $contenidoModel = new Services_Model_DbTable_Convenios();
        $identificador = $this->_getSanitizedParam("id");
        $this->_view->detalle = $contenidoModel->getById($identificador);
    }
}