<?php 
/**
*
*/
class Services_enviarclasificadoController extends Controllers_Abstract
{ 
	public function indexAction(){
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		$data = array();
		$data['clasificados_nombre'] = $this->_getSanitizedParam("clasificados_nombre");
		$data['clasificados_introduccion'] = $this->_getSanitizedParamHtml("clasificados_introduccion");
		$data['clasificados_descripcion'] = $this->_getSanitizedParamHtml("clasificados_descripcion");
		if($this->_getSanitizedParam("clasificados_categoria") == '' ) {
			$data['clasificados_categoria'] = '0';
		} else {
			$data['clasificados_categoria'] = $this->_getSanitizedParam("clasificados_categoria");
		}
		$data['clasificados_imagen'] = "";
		$data['clasificados_imagen1'] = "";
		$data['clasificados_imagen2'] = "";
		$data['clasificados_imagen3'] = "";
		$data['clasificados_imagen4'] = "";
		$data['clasificados_nombreusuario'] = $this->_getSanitizedParam("clasificados_nombreusuario");
		$data['clasificados_documento'] = $this->_getSanitizedParam("clasificados_documento");
		$data['clasificados_correo'] = $this->_getSanitizedParam("clasificados_correo");
		$data['clasificados_telefono'] = $this->_getSanitizedParam("clasificados_telefono");
		if($this->_getSanitizedParam("clasificados_estado") == '' ) {
			$data['clasificados_estado'] = '0';
		} else {
			$data['clasificados_estado'] = $this->_getSanitizedParam("clasificados_estado");
		}
		$uploadImage =  new Core_Model_Upload_Image();
		if($_FILES['clasificados_imagen']['name'] != ''){
			$data['clasificados_imagen'] = $uploadImage->upload("clasificados_imagen");
		}
		if($_FILES['clasificados_imagen1']['name'] != ''){
			$data['clasificados_imagen1'] = $uploadImage->upload("clasificados_imagen1");
		}
		if($_FILES['clasificados_imagen2']['name'] != ''){
			$data['clasificados_imagen2'] = $uploadImage->upload("clasificados_imagen2"); 
		}
		if($_FILES['clasificados_imagen3']['name'] != ''){
			$data['clasificados_imagen3'] = $uploadImage->upload("clasificados_imagen3");
		}
		if($_FILES['clasificados_imagen4']['name'] != ''){
			$data['clasificados_imagen4'] = $uploadImage->upload("clasificados_imagen4");
		}
		$clasificadoModel = new Page_Model_DbTable_Clasificados();
        $clasificado = $clasificadoModel->insert($data); 
        $email = new Core_Model_Sendingemail($this->_view); 
        $res = $email->confirmacion($data);
        echo $respuesta = json_encode(array('respuesta' =>$res));
	}
}