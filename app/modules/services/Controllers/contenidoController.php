<?php 

/**
*
*/

class Services_contenidoController extends Controllers_Abstract
{
    public function indexAction(){
        header('Access-Control-Allow-Origin: *');  
        $contenidoModel = new Services_Model_DbTable_Contenido();
        $data = [];
        $seccion = $this->_getSanitizedParam("seccion");
        $contenidos = $contenidoModel->getList("contenido_seccion = '$seccion' AND contenido_padre = '0'","orden ASC");
        foreach ($contenidos as $key => $contenido) {
            $subcontenidos = $contenidoModel->getList("contenido_padre = '".$contenido->contenido_id."'","orden ASC");
            $data[$key] = [];
            $data[$key]['detalle'] = $contenido;
            $data[$key]['subcontenidos'] = [];
            foreach ($subcontenidos as $key2 => $subcontenido) {
                $data[$key]['subcontenidos'][$key2] = [];
                $data[$key]['subcontenidos'][$key2]['detalle'] = $subcontenido;
                $data[$key]['subcontenidos'][$key2]['adicionales'] = $contenidoModel->getList("contenido_padre = '".$subcontenido->contenido_id."'","orden ASC");
            }
        }
        header('Content-Type: application/json');
        $this->_view->contenido = $data; 
    }

    public function detalleAction(){
        header('Access-Control-Allow-Origin: *'); 
        header('Content-Type: application/json');
        $contenidoModel = new Services_Model_DbTable_Contenido();
        $identificador = $this->_getSanitizedParam("id");
        $this->_view->detalle = $contenidoModel->getById($identificador);
    }
} 