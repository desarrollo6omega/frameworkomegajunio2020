<?php 

/**
*
*/

class Services_clasificadosController extends Controllers_Abstract
{
 
	public function indexAction()
	{
		header('Access-Control-Allow-Origin: *'); 
		header('Content-Type: application/json');
		$clasificadosModel = new Services_Model_DbTable_Clasificados();
		$filters = "clasificados_estado = '1'";
		$identificador = $this->_getSanitizedParam("id");
		if ($identificador>0 && $identificador!=="") {
			$filters = "clasificados_categoria = '$identificador'";
		} 
		$this->_view->clasificados = $clasificadosModel->getList("$filters","orden ASC");
	}

	public function categoriasAction(){
		header('Access-Control-Allow-Origin: *'); 
		header('Content-Type: application/json');
		$contentModel = new Services_Model_DbTable_Categorias();
		$this->_view->categoria = $contentModel->getList("","orden ASC");
	}	
	 public function detalleAction(){
        header('Access-Control-Allow-Origin: *'); 
        header('Content-Type: application/json');
        $contenidoModel = new Services_Model_DbTable_Clasificados();
        $identificador = $this->_getSanitizedParam("id");
        $this->_view->detalleclasificados = $contenidoModel->getById($identificador);
    }
}