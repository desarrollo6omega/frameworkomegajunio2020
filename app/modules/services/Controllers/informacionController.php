<?php 

/**
*
*/

class Services_informacionController extends Controllers_Abstract
{
    public function indexAction(){
        header('Access-Control-Allow-Origin: *');  
        $contenidoModel = new Services_Model_DbTable_Informacion();
        $this->_view->informacion = $contenidoModel->getList("","orden ASC");
        header('Content-Type: application/json');
    }
} 