<?php 

/**
* 
*/
class Services_Model_DbTable_Ingresos extends Db_Table
{
	protected $_name = 'ingresos';
	protected $_id = 'ingresos_id';

	public function insert($datainsert){
		$usuario = $datainsert['ingresos_usuario'];
		$fecha = $datainsert['ingresos_fecha'];
        $ingresos = $datainsert['ingresos_desde'];
		$query = "INSERT INTO ingresos (ingresos_usuario, ingresos_fecha, ingresos_desde) VALUES ( '$usuario', '$fecha', '$ingresos')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}
	public function getListJoin($filters = '',$order = '')
    {
        $filter = '';
        if($filters != ''){
            $filter = ' WHERE '.$filters;
        }
        $orders ="";
        if($order != ''){
            $orders = ' ORDER BY '.$order;
        } 
        $select = 'SELECT * FROM ('.$this->_name.' LEFT JOIN user ON user_id = ingresos_usuario '.$filter.' '.$orders;
        $res = $this->_conn->query( $select )->fetchAsObject();
        return $res;
    } 
}