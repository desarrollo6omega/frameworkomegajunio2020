<?php 

/**
* 
*/
class Page_Model_DbTable_Pqr extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'pqr';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
    protected $_id = 'pqr_id';
    
    public function insert($data){
		$fecha =$data['fecha'];
		$nombre = $data['nombre'];
		$direccion = $data['direccion'];
		$telefono = $data['telefono'];
        $celular = $data['celular'];
        $area = $data['area'];
		$email = $data['email'];
		$motivo = $data['motivo'];
		$descripcion = $data['descripcion'];
		$nombrefuncionario = $data['nombrefuncionario'];
        $excelente = $data['excelente'];
        $bueno = $data['bueno'];
        $regular = $data['regular'];
		$query = "INSERT INTO pqr(pqr_fecha, pqr_nombre, pqr_direccion, pqr_telefono, pqr_celular, pqr_area, pqr_email, pqr_motivo, pqr_descripcion, pqr_nombrefuncionario, pqr_excelente, pqr_bueno, pqr_regular ) VALUES ( '$fecha', '$nombre', '$direccion', '$telefono', '$celular', '$area' , '$email', '$motivo', '$descripcion', '$nombrefuncionario','$excelente','$bueno','$regular' )";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}
}