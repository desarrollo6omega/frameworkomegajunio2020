<?php

/**
 *
 */

class Page_formatosController extends Page_mainController
{


	public function indexAction()
	{
		$productosModel = new Page_Model_DbTable_Formatos();
		$informacionModel = new Page_Model_DbTable_Informacion();
		$lateral = $this->_view->getRoutPHP('modules/page/Views/partials/lateral.php');
		$this->_view->informacion = $informacionModel->getList("", "orden ASC")[0];
		$filterbuscador = "formato_estado='1'";
		$this->_view->cat = $categoria = $this->_getSanitizedParam("categoria");
		if ($this->_getSanitizedParam("buscador")) {
			$buscador = $this->_getSanitizedParam("buscador");
			$filterbuscador = " formato_nombre LIKE '%$buscador%' AND formato_estado='1'";
		}
		if ($categoria) {

			$filters = " formato_subcategoria = '$categoria' AND formato_estado='1' ";
			$order = "orden ASC";
			$list = $productosModel->getList($filters, $order);
			$amount = 9;
			$page = $this->_getSanitizedParam("page");
			if (!$page) {
				$start = 0;
				$page = 1;
			} else {
				$start = ($page - 1) * $amount;
			}
			$this->_view->totalpages = ceil(count($list) / $amount);
			$this->_view->page = $page;
			$this->_view->productos = $productosModel->getListPages($filters, $order, $start, $amount);
		} else {
			$this->_view->productos = $productosModel->getList($filterbuscador, "orden ASC");
		}
	}
}
