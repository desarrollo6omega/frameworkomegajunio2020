<?php 

/**
*
*/

class Page_albumController extends Page_mainalbumController
{

	public function indexAction()
	{
		
		
		$modelAlbum = new Page_Model_DbTable_Album();
		$modelFotos = new Page_Model_DbTable_Foto();
		$album = $this->_getSanitizedParam("album");
		$contentModel = new Page_Model_DbTable_Content();
		$infopageModel = new Page_Model_DbTable_Informacion();
		$ciudadesModel = new Page_Model_DbTable_Ciudades();
		$categoriasconveniosModel = new Page_Model_DbTable_Conveniocategoria();
		$informacionalbum = $modelAlbum->getById($album);
		$this->_view->album = $informacionalbum;
		$this->_view->infopage = $infopageModel->getById(1);
		$album_titulo = $informacionalbum->album_titulo;
		$album_descripcion = $informacionalbum->album_descripcion;
		$album_imagen = $informacionalbum->album_imagen;
		$album_id = $informacionalbum->album_id;
		$this->_view->institucional = $contentModel->getList("content_section = 'Institucional' AND content_state = '1'","orden ASC");
		$this->_view->asociado = $contentModel->getList(" content_section = 'Como ser asociado'","orden ASC");
		$this->_view->ahorro = $contentModel->getList(" content_section = 'Ahorro' AND content_state = '1'","orden ASC");
		$this->_view->creditos = $contentModel->getList(" content_section = 'Creditos' AND content_state = '1'","orden ASC");
		$this->_view->seguros = $contentModel->getList(" content_section = 'Seguros' AND content_state = '1'","orden ASC");
		$this->_view->turismo = $ciudadesModel->getList("","orden ASC");
		$this->_view->beneficios = $contentModel->getList(" content_section = 'Beneficios' AND content_state = '1'","orden ASC");
		$this->_view->categoriasconvenios = $categoriasconveniosModel->getList("","orden ASC");
		$this->_view->bienestar = $contentModel->getList(" content_section = 'Bienestar' AND content_state = '1'","orden ASC");
		$this->_view->fecolsaenlinea = $contentModel->getList("content_section = 'Fecolsa en linea'","orden ASC");
		$this->_view->certificados = $contentModel->getList(" content_section = 'Certificados' AND content_state = '1'","orden ASC");
		$this->_view->fotos = $modelFotos->getList("album_id = '$album'"," orden ASC");
		
		$this->getLayout()->setData("idgaleria",$album_id);
		$this->getLayout()->setData("titulogaleria",$album_titulo);
		$this->getLayout()->setData("descripciongaleria",$album_descripcion);
		$this->getLayout()->setData("imagengaleria",$album_imagen);
	}
} 