<?php 

/**
*
*/

class Page_disciplinasController extends Page_mainController
{

	public function indexAction()
	{
		$disciplinasModel = new Page_Model_DbTable_Disciplinas();
		$disciplinascategoriaModel = new Page_Model_DbTable_Disciplinascategoria();
        $categorias = $disciplinascategoriaModel->getList("disciplinas_categoria_estado = '1'", "orden ASC");
		$this->_view->contenido = $this->template->getContentseccion(3);
		$array = array();
		foreach ($categorias as $key => $categoria) {
			$identificador = $categoria->disciplinas_categoria_id;
			$array[$key] = [];
			$array[$key]['detalle'] = $categoria;
			$array[$key]['disciplinas'] = $disciplinasModel->getList("disciplinas_categoria = '$identificador' AND disciplinas_estado = '1'", "orden ASC");
		}
		$this->_view->categorias = $array; 
	}
}