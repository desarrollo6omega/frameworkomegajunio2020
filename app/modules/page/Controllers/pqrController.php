<?php 

/**
*
*/

class Page_pqrController extends Page_mainController
{

	public function indexAction()
	{
		$userModel = new Page_Model_DbTable_Usuario();
		$id = Session::getInstance()->get("kt_login_id","");
		$this->_view->buscar = $userModel->getById($id);
		$this->_view->res = $this->_getSanitizedParam('res');
		$this->_view->pqr= $this->template->getContentseccion(11);
		$fecha = date("Y-m-d");
		$this->_view->fecha = $fecha;
	}
	public function enviar1Action()
	{
		$this->setLayout('blanco');
		$data = [''];
		$data ['fecha'] = $this->_getSanitizedParam('fecha'); 
		$data ['nombre'] = $this->_getSanitizedParam('nombre');
        $data ['direccion'] = $this->_getSanitizedParam('direccion');
        $data ['telefono'] = $this->_getSanitizedParam('telefono');
		$data ['celular'] = $this->_getSanitizedParam('celular');
		$data ['area'] = $this->_getSanitizedParam('area');
		$data ['email'] = $this->_getSanitizedParam('email');
		$data ['motivo'] = $this->_getSanitizedParam('motivo');
		$data ['descripcion'] = $this->_getSanitizedParam('descripcion');
		$data ['nombrefuncionario'] = $this->_getSanitizedParam('nombrefuncionario');
		$data ['excelente'] = $this->_getSanitizedParam('excelente');
		$data ['bueno'] = $this->_getSanitizedParam('bueno');
		$data ['regular'] = $this->_getSanitizedParam('regular');
		$email = new Core_Model_Sendingemail($this->_view);  
		$res = $email->enviarcorreo1($data);
		header("Location: /page/pqr?res=".$res);
		$pqrModel = new Page_Model_DbTable_Pqr();
		$pqr = $pqrModel->insert($data);
	}	

}