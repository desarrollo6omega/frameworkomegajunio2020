<?php 

/**
*
*/

class Page_galeriaController extends Page_mainController 
{

	public function indexAction()
	{
		
		$infopageModel = new Page_Model_DbTable_Informacion();
		$modelAlbum = new Page_Model_DbTable_Album();
		$modelFotos = new Page_Model_DbTable_Foto();
		$modelPublicidad = new Page_Model_DbTable_Publicidad();
		$modelCiudadgaleria = new Page_Model_DbTable_Ciudadgaleria();
		$modelArea = new Page_Model_DbTable_Area();
		$this->_view->areas = $modelArea->getList("","orden ASC");
		$this->_view->ciudad = $modelCiudadgaleria->getList("","orden ASC");
		$this->_view->banner = $modelPublicidad->getList("publicidad_seccion = '5'","orden ASC");
		$this->_view->albumes = $modelAlbum->getList("","orden ASC");
		$infopage = $infopageModel->getById(1);
		$this->_view->infopage = $infopage;
		$filters = "";
		$order = "orden DESC";
		$list = $modelAlbum->getList($filters,$order);
		$amount = 12;
		$page = $this->_getSanitizedParam("page"); 
		if (!$page) {
			   	$start = 0;
			   	$page=1;
		} else {
			   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->albumes =  $modelAlbum->getListPages($filters,$order,$start,$amount);
		

	}
	public function buscadorAction(){
		
		$infopageModel = new Page_Model_DbTable_Informacion();
		$modelAlbum = new Page_Model_DbTable_Album();
		$modelCiudadgaleria = new Page_Model_DbTable_Ciudadgaleria();
		$modelPublicidad = new Page_Model_DbTable_Publicidad();
		$modelArea = new Page_Model_DbTable_Area();
		$ciudad = $this->_getSanitizedParam("ciudad");
		$area = $this->_getSanitizedParam("area");
		$infopage = $infopageModel->getById(1);
		$this->_view->infopage = $infopage;
		$this->_view->banner = $modelPublicidad->getList("publicidad_seccion = '5'","orden ASC");
		$this->_view->areas = $modelArea->getList("","orden ASC");
		$this->_view->ciudades = $modelCiudadgaleria->getList("","orden ASC");

		$this->_view->banner = $modelPublicidad->getList("publicidad_seccion = '5'","orden ASC");
		
		if($ciudad != "" && $area != ""){
			$filters = " area_id = '$area' AND ciudad_id = '$ciudad'";
		} else if($ciudad){
			$filters = "ciudad_id = '$ciudad' ";
		} else if($area){
			$filters = " area_id = '$area' ";
		}
		
		$order = "orden DESC";
		$list = $modelAlbum ->getList($filters,$order);
		$amount = 12;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->area = $this->_getSanitizedParam("area");
		$this->_view->ciudad = $this->_getSanitizedParam("ciudad");
		$this->_view->albumes =  $modelAlbum->getListPages($filters,$order,$start,$amount);
		
	}
} 