<?php 

/**
*
*/

class Page_conveniosController extends Page_mainController
{

	public function indexAction()
	{
		$conveniosModel = new Page_Model_DbTable_Convenios();
		$convenioscategoriaModel = new Page_Model_DbTable_Convenioscategoria();
        $categorias = $convenioscategoriaModel->getList("convenios_categoria_estado = '1'", "orden ASC");
		$this->_view->contenido = $this->template->getContentseccion(3);
		$array = array();
		foreach ($categorias as $key => $categoria) {
			$identificador = $categoria->convenios_categoria_id;
			$array[$key] = [];
			$array[$key]['detalle'] = $categoria;
			$array[$key]['convenios'] = $conveniosModel->getList("convenios_categoria = '$identificador' AND convenios_estado = '1'", "orden ASC");
		}
		$this->_view->categorias = $array; 
	}
}