<div class="padding-crediciti">
    <div class="crediciti">
        <div><img src="/images/<?php echo $contenido->contenido_imagen; ?>"></div>
        <div class="fondo-gris">
            <div><h2><?php echo $contenido->contenido_titulo; ?></h2></div>
            <div class="descripcion"><?php echo $contenido->contenido_descripcion; ?></div>
            <?php if($contenido->contenido_enlace){ ?>
                <div class="boton-crediciti">
                    <a href="<?php echo $contenido->contenido_enlace; ?>" <?php if($contenido->contenido_enlace_abrir == 1){ ?> target="_blank" <?php } ?>   class="btn btn-vermas"> <?php if( $contenido->contenido_vermas){ ?><?php echo $contenido->contenido_vermas; ?><?php } else { ?>Ver Más<?php } ?></a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
