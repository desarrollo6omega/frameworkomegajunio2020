<div class="titulo-internas" style="background-image:url(/skins/page/images/LogoLigadePatinaje.png)">
    <div align="center"><h2>Formato P.Q.R.S</h2></div>
</div>
<?php echo $this->pqr; ?>
<?php  $this->res; ?>
<div class="container">
    <?php if ($_SESSION['kt_login_id'] > 0) { ?>
        <?php  if($this->res == 1 ) { ?>
            <div align="center" class="alert alert-success"> El mensaje se envió satisfactoriamente, muy pronto nos pondremos en contacto contigo.</div>
        <?php } else if ($this->res == 2) {?>
            <div align="center" class="alert alert-danger">EL mensaje no se pudo enviar, intente de nuevo</div>
        <?php } ?> 
        <div class="formulario-pqr">
            <form method="POST" action="/page/pqr/enviar1">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Fecha de Presentación</label>
                    <input name="fecha" value="<?php echo $this->fecha; ?>" type="date" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Nombres y Apellidos</label>
                    <input name="nombre"type="text" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Dirección</label>
                    <input name="direccion"type="text" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Teléfono</label>
                    <input name="telefono"type="text" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Celular</label>
                    <input name="celular"type="text" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Área</label>
                    <input name="area" type="text" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">E-mail</label>
                    <input name="email"type="email" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Motivo</label>
                    <select name="motivo"class="form-control" id="exampleFormControlSelect1">
                        <option>Seleccione...</option>
                        <option value="peticion">Petición</option>
                        <option value="queja">Queja</option>
                        <option value="reclamo">Reclamo</option>
                        <option value="sugerencia">Sugerencia</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Descripción de la Situación</label>
                    <textarea style="resize:none;" name="descripcion"class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Nombre del funcionario que lo atendió</label>
                    <input name="nombrefuncionario"type="text" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                </div>
                <label for="exampleFormControlInput1">Califique el Servicio</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="excelente" id="exampleRadios1" value="excelente" checked>
                    <label class="form-check-label" for="exampleRadios1">
                        Excelente
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="bueno" id="exampleRadios2" value="bueno">
                    <label class="form-check-label" for="exampleRadios2">
                        Bueno
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="regular" id="exampleRadios2" value="regular">
                    <label class="form-check-label" for="exampleRadios2">
                        Regular
                    </label>
                </div> </br>
                <div class="form-group">
                    <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck" required>
                    <label class="form-check-label" for="gridCheck">
                        Acepto terminos y condiciones
                    </label>
                    </div>
                </div>
                <div class="g-recaptcha" data-sitekey="6LdIX44UAAAAADuYlD4cxeLDe6VHvSQhChRCTh4E"></div>
                <div class="boton-pqr"><button type="submit" class="btn btn-primary">Enviar</button></div>
            </form>
        </div>
    <?php } else { ?>
        <div class="alert alert-primary" align="center" role="alert">
            Señor(a) Asociado, para realizar la inscripción a nuestros eventos debe iniciar sesión ingresando <a data-toggle="modal" data-target="#exampleModal">Aquí</a>.
        </div>
    <?php } ?>
</div>