<?php $this->res; ?>
<div class="titulo-internas" style="background-image:url(/skins/page/images/LogoLigadePatinaje.png)">
    <div align="center">
        <h2>Contacto</h2>
    </div>
</div>
<div class="contacto">
    <div class="container">
        <div class="col-sm-12">
            <?php if ($this->res == 1) { ?>
                <div align="center" class="alert alert-success"> El mensaje se envió satisfactoriamente, muy pronto nos pondremos en contacto contigo.</div>
            <?php } else if ($this->res == 2) { ?>
                <div align="center" class="alert alert-danger">EL mensaje no se pudo enviar, intente de nuevo</div>
            <?php } ?>
            <form method="POST" action="/page/contacto/enviar" onsubmit="return miFuncion(this)">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-row">
                            <div class="form-group col-md-12 cont_sin_espacio">
                                <label for="inputEmail4"></label>
                                <input type="text" name="nombre" class="form-control" id="inputEmail4" placeholder="Nombre:" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 cont_sin_espacio">
                                <label for="inputEmail4"></label>
                                <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="E-mail:" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 cont_sin_espacio">
                                <label for="inputEmail4"></label>
                                <input type="text" name="telefono" class="form-control" id="inputEmail4" placeholder="Teléfono:" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 cont_sin_espacio">
                                <label for="inputEmail4"></label>
                                <input type="text" name="ciudad" class="form-control" id="inputEmail4" placeholder="Ciudad:" required>
                            </div>
                            <div class="form-group col-md-12 cont_sin_espacio"> <label for="inputEmail4"></label>
                                <textarea style="resize:none;" class="form-control" name="mensaje" id="" rows="4" placeholder="Mensaje:" required></textarea>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="informacion-contacto-noresponsive">
                            <div class="informacion-contacto">
                                <h3>Información de Contacto</h3><?php echo $this->infopage->info_pagina_informacion_contacto_footer ?>
                            </div>
                            <div class="redes-contactenos"><span>Síguenos en:</span>
                                <?php if ($this->infopage->info_pagina_facebook) { ?>
                                    <a href="<?php echo $this->infopage->info_pagina_facebook ?>" target="_blank" class="red">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                <?php } ?>
                                <?php if ($this->infopage->info_pagina_twitter) { ?>
                                    <a href="<?php echo $this->infopage->info_pagina_twitter ?>" target="_blank" class="red">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                <?php } ?>
                                <?php if ($this->infopage->info_pagina_instagram) { ?>
                                    <a href="<?php echo $this->infopage->info_pagina_instagram ?>" target="_blank" class="red">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                <?php } ?>
                                <?php if ($this->infopage->info_pagina_pinterest) { ?>
                                    <a href="<?php echo $this->infopage->info_pagina_pinterest ?>" target="_blank" class="red">
                                        <i class="fab fa-pinterest-p"></i>
                                    </a>
                                <?php } ?>
                                <?php if ($this->infopage->info_pagina_youtube) { ?>
                                    <a href="<?php echo $this->infopage->info_pagina_youtube ?>" target="_blank" class="red">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                <?php } ?>
                                <?php if ($this->infopage->info_pagina_linkdn) { ?>
                                    <a href="<?php echo $this->infopage->info_pagina_linkdn ?>" target="_blank" class="red">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                <?php } ?>
                                <?php if ($this->infopage->info_pagina_google) { ?>
                                    <a href="<?php echo $this->infopage->info_pagina_google ?>" target="_blank" class="red">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                <?php } ?>
                                <?php if ($this->infopage->info_pagina_flickr) { ?>
                                    <a href="<?php echo $this->infopage->info_pagina_flickr ?>" target="_blank" class="red">
                                        <i class="fab fa-flickr"></i>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="form-check" required style="margin-top: 2%;">
                        <input class="form-check-input" type="checkbox" id="gridCheck" required>
                        <label class="form-check-label" for="gridCheck">
                            <a class="pol_datos" data-toggle="modal" data-target="#myModal">Política de Manejo de Datos.</a>

                        </label>
                    </div>
                </div>
                <div class="g-recaptcha" data-sitekey="6LeYGYkUAAAAALAIl2Rr4k2yeeqTFDDsdG8pVomo"></div>
                <div class="boton-contactenos"><button name="enviar" type="submit" class="btn btn-primary">Enviar</button></div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="max-width: 750px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= $this->infopage->info_pagina_titulo_legal; ?></h4>
                <button type="button" class="close" data-dismiss="modal" style="margin-top: -10px;">&times;</button>

            </div>

            <div class="modal-body">
                <p><?= $this->infopage->info_pagina_descripcion_legal; ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>
<div class="mapa">
    <script type="text/javascript">
        setValuesMap('<?php echo $this->infopage->info_pagina_latitud ?>', '<?php echo $this->infopage->info_pagina_longitud ?>', true, 18);
        google.maps.event.addDomListener(window, 'load', initializeMap);
    </script>
    <div id="map" style="width:100%; height:100%;display: inline-block;"></div>
</div>
<script>
    function miFuncion(a) {
        var response = grecaptcha.getResponse();

        if (response.length == 0) {
            alert("Captcha no verificado");
            return false;
            event.preventDefault();
        } else {
            return true;
        }
    }
</script>