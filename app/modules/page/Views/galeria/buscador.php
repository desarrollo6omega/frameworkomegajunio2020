<?php
    function formatoDMY($f, $opcion){
        $dia = explode("-", $f, 3);
        $year = $dia[0];
        $month = (string)(int)$dia[1];
        $day = (string)(int)$dia[2];
        $dias = array("domingo","lunes","martes","mi&eacute;rcoles" ,"jueves","viernes","s&aacute;bado");
        $tomadia = $dias[intval((date("w",mktime(0,0,0,$month,$day,$year))))];
        $meses = array("", "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");
        switch ($opcion) {
        case "diasemana":
        echo $tomadia;
        break;
        case "diames":
        echo str_pad($day,2,'0',STR_PAD_LEFT);
        break;
        case "mes":
        echo $meses[$month];
        break;
        case "anio":
        echo $year;
        break;
        default:
        echo "Nada";
        } 
    }
?> 
<div class="caja-ahorro">
    <div class="container" >
        <div align="left"><h3>Galería</h3></div>
    </div>
</div> 
<div align="center">
	<?php if($this->banner[0]->publicidad_imagen){ ?>
		<div class="imagen-fondo" style="background-image: url(/<?php echo $this->banner[0]->publicidad_imagen; ?>);"></div>
		<div class="imagen-fondo-responsive" style="background-image: url(/<?php echo $this->banner[0]->publicidad_imagen; ?>);"></div>
	<?php } else { ?>
		<div class="imagen-fondo" style="background-image: url(/skins/page/images/banner-interna.jpg);"></div>
	<?php } ?>
	<div class="container">
		<div class="interna">
			<form action="/page/galeria/buscador" method="post">
			  	<div class="row">
			  		
			    	<div class="col-md-3 mt-5">
			      		<select class="form-control" name="area">
  							<option value=" "> Aréa</option>
  							<?php foreach ($this->areas as $key => $area): ?>
  								<option value="<?php echo $area->area_id?>"><?php echo $area->area_titulo?></option>	
  							<?php endforeach ?>
						</select>
			    	</div>
			    	<div class="col-md-3 mt-5">
			      		<select class="form-control" name="ciudad">
  							<option value=" "> Ciudad</option>
  							<?php foreach ($this->ciudades as $key => $ciudad): ?>
  								<option value="<?php echo $ciudad->ciudadgaleria_id?>"><?php echo $ciudad->ciudadgaleria_titulo?></option>	
  							<?php endforeach ?>
						</select>
			    	</div>
			    	 <button type="submit" class="btn btn-primarym mt-5 mb-2">Filtrar</button>
			  	</div>
			</form>
			<div class="row">
				<?php foreach ($this->albumes as $key => $album): ?>
					<div class="col-md-4 mt-3 mb-5">
						<div class="cuadro-galeria">
							<a href="/page/album?album=<?php echo $album->album_id;?>"><div class="cuadro-imagen-galeria" style="background-image: url(/images/<?php echo str_replace(" ","%20",$album->album_imagen); ?>);"></div></a>
							<h3>
								<div><a href="/page/album?album=<?php echo $album->album_id;?>"><?php echo $album->album_titulo; ?></a></div>
							</h3>
							<div class="contenedor-bajo">
								<div class="fecha">
									<?php $f = $album->album_fecha; ?>
									<?php echo formatoDMY($f, "diames");  ?> <?php echo formatoDMY($f, "mes"); ?> <?php echo formatoDMY($f, "anio");?>
								</div>
								<div class="compartir">
                                        <span>
                                            Compartir
                                        </span>
                                        <a href="whatsapp://send?text=<?php echo $_SERVER['HTTP_HOST'] ?>/page/album?album=<?php echo $album->album_id;?>" target="_blank" class="red2">
                                            <i class="fab fa-whatsapp"></i>
                                        </a>
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//<?php echo $_SERVER['HTTP_HOST'] ?>/page/album?album=<?php echo $album->album_id;?>" target="_blank" class="red2">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                </div>
							</div>	
						</div>
					</div>
				<?php endforeach ?>
				<div align="center">
					<nav class="navegacion" aria-label="Page navigation example">
						<ul class="pagination">
						    <?php
								if ($this->totalpages < 10) {
									$paginainicial = 1;
									$paginafinal = $this->totalpages;
								}
								else {
									 if ($this->page<5) {
										$paginainicial = 1;
										$paginafinal = 9;
									}
									else if ($this->page > ($this->totalpages-4)) {
										$paginainicial = $this->totalpages-9;
										$paginafinal = $this->totalpages;
									}
									else {
										$paginainicial = $this->page-3;
										$paginafinal = $this->page+3;
									}
								}
						    	$url = 'buscador';
						        if ($this->totalpages > 1) {
						            if ($this->page != 1)
						                echo '<li class="page-item "><a href="'.$url.'?page='.($this->page-1).'&area='.$this->area.'&ciudad='.$this->ciudad.'" class="page-link"> &laquo;</a></li>';
						            for ($i=$paginainicial;$i<=$paginafinal;$i++) {
						                if ($this->page == $i)
						                    echo '<li class="page-item active"><a class="page-link">'.$this->page.'</a></li>';
						                else
						                    echo '<li class="page-item"><a class="page-link" href="'.$url.'?page='.$i.'&area='.$this->area.'&ciudad='.$this->ciudad.'">'.$i.'</a></li>  ';
						            }
						            if ($this->page != $this->totalpages)
						                echo '<li class="page-item"><a class="page-link" href="'.$url.'?page='.($this->page+1).'&area='.$this->area.'&ciudad='.$this->ciudad.'">&raquo;</a></li>';
						        }
						  ?>
					  	</ul>
				  	</nav>
				</div>
			<div> 
		</div>
	</div>	
</div>
