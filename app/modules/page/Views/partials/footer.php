<div class="fondo-gris">
	<div class="container">
		<div class="preguntas-frecuentes">
			<div><a href="/page/preguntasfrecuentes">Preguntas Frecuentes <i class="fas fa-question"></i></a></div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div><h3>CONTÁCTANOS</h3></div>
				<div><?php echo $this->infopage->info_pagina_informacion_contacto_footer; ?></div>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div><h3>ENLACES DE INTERÉS</h3></div>
				<div class="enlaces">
					<ul>	
						<?php foreach ($this->enlaces as $key => $enlace) { ?>
							<li><a href="<?php echo $enlace->enlaces_link ?>" target="blank"><?php echo $enlace->enlaces_titulo ?></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div><h3>SÍGUENOS EN:</h3></div>
				<div class="redes-footer">
					<?php if($this->infopage->info_pagina_facebook) {?>
						<a href="<?php echo $this->infopage->info_pagina_facebook ?>" target="_blank" class="red">
							<i class="fab fa-facebook-f"></i>
						</a>
					<?php } ?>
					<?php if($this->infopage->info_pagina_twitter) {?>
						<a href="<?php echo $this->infopage->info_pagina_twitter ?>" target="_blank" class="red">
							<i class="fab fa-twitter"></i>
						</a>
					<?php } ?>
					<?php if($this->infopage->info_pagina_instagram) {?>
						<a href="<?php echo $this->infopage->info_pagina_instagram ?>" target="_blank" class="red">
							<i class="fab fa-instagram"></i>
						</a>
					<?php } ?>
					<?php if($this->infopage->info_pagina_youtube) {?>
						<a href="<?php echo $this->infopage->info_pagina_youtube ?>" target="_blank" class="red">
							<i class="fab fa-youtube"></i>
						</a>
					<?php } ?>
					<?php if($this->infopage->info_pagina_pinterest) {?>
						<a href="<?php echo $this->infopage->info_pagina_pinterest ?>" target="_blank" class="red">
							<i class="fab fa-pinterest-p"></i>
						</a>
					<?php } ?>
					<?php if($this->infopage->info_pagina_linkedin) {?>
						<a href="<?php echo $this->infopage->info_pagina_linkedin ?>" target="_blank" class="red">
							<i class="fab fa-linkedin-in"></i>
						</a>
					<?php } ?>
					<?php if($this->infopage->info_pagina_google) {?>
						<a href="<?php echo $this->infopage->info_pagina_google ?>" target="_blank" class="red">
							<i class="fab fa-google-plus-g"></i>
						</a>
					<?php } ?>
					<?php if($this->infopage->info_pagina_flickr) {?>
						<a href="<?php echo $this->infopage->info_pagina_flickr ?>" target="_blank" class="red">
							<i class="fab fa-flickr"></i>
						</a>
					<?php } ?>
				</div>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-3 d-none">
				<div class="logo-blanco">
					<div><img src="/skins/page/images/logo.png" alt=""></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="fondo-verde" align="center">
	&copy; Todos los Derechos Reservados <?php echo date('Y'); ?> - Diseñado por Omega Soluciones Web
</div>