<div class="header-redes">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<!--<?php if ($this->infopage->info_pagina_telefono) { ?>
					<?php $telefono = intval(preg_replace('/[^0-9]+/', '', $this->infopage->info_pagina_telefono), 10);  ?>
					<a href="tel:<?php echo $telefono; ?>" target="_blank" class="red">
						<i class="fas fa-phone"></i>
						<span><?php echo $this->infopage->info_pagina_telefono ?></span>
					</a>
				<?php } ?>-->
				<?php if ($this->infopage->info_pagina_facebook) { ?>
					<a href="<?php echo $this->infopage->info_pagina_facebook ?>" target="_blank" class="red">
						<i class="fab fa-facebook-f"></i>
					</a>
				<?php } ?>
				<?php if ($this->infopage->info_pagina_twitter) { ?>
					<a href="<?php echo $this->infopage->info_pagina_twitter ?>" target="_blank" class="red">
						<i class="fab fa-twitter"></i>
					</a>
				<?php } ?>
				<?php if ($this->infopage->info_pagina_instagram) { ?>
					<a href="<?php echo $this->infopage->info_pagina_instagram ?>" target="_blank" class="red">
						<i class="fab fa-instagram"></i>
					</a>
				<?php } ?>
				<?php if ($this->infopage->info_pagina_youtube) { ?>
					<a href="<?php echo $this->infopage->info_pagina_youtube ?>" target="_blank" class="red">
						<i class="fab fa-youtube"></i>
					</a>
				<?php } ?>
				<?php if ($this->infopage->info_pagina_pinterest) { ?>
					<a href="<?php echo $this->infopage->info_pagina_pinterest ?>" target="_blank" class="red">
						<i class="fab fa-pinterest-p"></i>
					</a>
				<?php } ?>
				<?php if ($this->infopage->info_pagina_linkedin) { ?>
					<a href="<?php echo $this->infopage->info_pagina_linkedin ?>" target="_blank" class="red">
						<i class="fab fa-linkedin-in"></i>
					</a>
				<?php } ?>
				<?php if ($this->infopage->info_pagina_google) { ?>
					<a href="<?php echo $this->infopage->info_pagina_google ?>" target="_blank" class="red">
						<i class="fab fa-google-plus-g"></i>
					</a>
				<?php } ?>
				<?php if ($this->infopage->info_pagina_flickr) { ?>
					<a href="<?php echo $this->infopage->info_pagina_flickr ?>" target="_blank" class="red">
						<i class="fab fa-flickr"></i>
					</a>
				<?php } ?>
				<?php if ($this->infopage->info_pagina_whatsapp) { ?>
					<?php $whatsapp = intval(preg_replace('/[^0-9]+/', '', $this->infopage->info_pagina_whatsapp), 10);  ?>
					<a href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp; ?>" target="_blank" class="red">
						<i class="fab fa-whatsapp"></i>
						<span><?php echo $this->infopage->info_pagina_whatsapp ?></span>
					</a>
				<?php } ?>
				<!--<a href="mailto:<?php echo $this->infopage->info_pagina_correos_contacto; ?>" target="_blank" class="red">
					<i class="far fa-envelope"></i>
					<span><?php echo $this->infopage->info_pagina_correos_contacto; ?></span>
				</a>-->
			</div>
			<div class="col-sm-4 text-right">
				<div class="red">
					<span><?php if ($this->infopage->info_pagina_telefono) { ?>
							<?php $telefono = intval(preg_replace('/[^0-9]+/', '', $this->infopage->info_pagina_telefono), 10);  ?>
							<a href="tel:<?php echo $telefono; ?>" target="_blank" class="red">
								<i class="fas fa-phone"></i>
								<span><?php echo $this->infopage->info_pagina_telefono ?></span>
							</a>
						<?php } ?></span>
				</div>
				<div class="red">
					<span><a href="mailto:<?php echo $this->infopage->info_pagina_correos_contacto; ?>" target="_blank" class="red">
							<i class="far fa-envelope"></i>
							<span><?php echo $this->infopage->info_pagina_correos_contacto; ?></span>
						</a></span>
				</div>


			</div>
		</div>
	</div>
</div>

</div>

<div class="header-content">
	<div class="container">
		<div class="botonera">
			<div class="row no-gutters">
				<div class="col-sm-3">
					<a href="/page/index">
						<div align="center"><img src="/skins/page/images/logo.png" class="logo"></div>
					</a>
				</div>
				<div class="col-9">
					<nav>
						<ul>
							<!--<li><a href="/page/index"><span>Inicio</span></a></li>-->

							<li><a href="/page/nosotros"><span>Liga de Bogotá<i class="fas fa-angle-down d-none"></i></span></a>
								<ul>
									<?php foreach ($this->botonesnosotros as $key => $nosotros) { ?>
										<li><a href="/page/nosotros#<?php echo $nosotros->contenido_id ?>"><i class="fas fa-caret-right"></i>&nbsp;&nbsp;<?php echo $nosotros->contenido_titulo ?></a></li>
									<?php } ?>
								</ul>
							</li>
							<li><a href="/page/asociarse"><span>Asociarse</span></a></li>
							<li><a href="/page/escuelaformacion"><span>Escuela de Formación</span></a></li>
							<li><a href="#"><span>Convenios y Disciplinas <i class="fas fa-angle-down d-none"></i></span></a>
								<ul class="ul_servicios">
									<?php if (count($this->convenios) != 0) { ?>

										<li><a href="/page/convenios"><i class="fas fa-caret-right"></i>&nbsp;&nbsp;Convenios</a></li>
									<?php } ?>

									<?php if (count($this->disciplinas) != 0) { ?>
										<li><a href="/page/disciplinas"><i class="fas fa-caret-right"></i>&nbsp;&nbsp;Disciplinas</a></li>
									<?php } ?>

								</ul>

							</li>
							<li class="item"><a href="/page/noticias"><span style="    padding-left: 12px;">Noticias</span></a></li>

							<li class="item"><a href="/page/galeria"><span style="    padding-left: 12px;">Galería</span></a></li>

							<li class="item"><a href="/page/contacto"><span>Contacto</span></a></li>
						</ul>
					</nav>
				</div>

			</div>
		</div>
		<div class="botonera-responsive">
			<div class="row no-gutters">
				<div class="col-sm-3">
					<div><a href="/"><img src="/skins/page/images/logo.png" class="logo"></a></div>
				</div>
				<div class="col-sm-9">
					<div>
						<?php if ($_SESSION['kt_login_id'] == "") { ?>
							<a href="" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-user d-none"></i></a>
						<?php } ?>
						<a class="menu-responsive"><i class="fas fa-bars"></i></a>
					</div>
					<nav>

						<div><a class="menu-responsive"><i class="fas fa-times-circle"></i></a></div>
						<ul>
							<li><a href="/page/index"><span>Inicio</span></a></li>
							<li><a id="liga" onclick="mostrar()"><span>Liga de Bogotá <i class="fas fa-angle-down"></i></span></a>
								<ul id="nosotros">
									<?php foreach ($this->botonesnosotros as $key => $nosotros) { ?>
										<li><a href="/page/nosotros#<?php echo $nosotros->contenido_id ?>"><?php echo $nosotros->contenido_titulo ?></a></li>
									<?php } ?>
								</ul>
							</li>
							<li><a href="/page/asociarse"><span>Asociarse</span></a></li>
							<li><a href="/page/convenios"><span>Convenios y Disciplinas</span></a>
								<ul class="ul_servicios">
									<li><a href="/page/convenios">Convenios</a></li>
								</ul>
							</li>
							<li class="item"><a class="borde" href="/page/galeria"><span>Galeria</span></a></li>

							<li class="item"><a href="/page/noticias"><span>Noticias</span></a>

							</li>
							<li class="item"><a class="borde" href="/page/contacto"><span>Contácto</span></a></li>
						</ul>
						<div class="redes">
							<?php if ($this->infopage->info_pagina_telefono) { ?>
								<?php $telefono = intval(preg_replace('/[^0-9]+/', '', $this->infopage->info_pagina_telefono), 10);  ?>
								<a href="tel:<?php echo $telefono; ?>" target="_blank" class="red">
									<i class="fas fa-phone"></i>
									<!--<span><?php echo $this->infopage->info_pagina_telefono ?></span>-->
								</a>
							<?php } ?>
							<?php if ($this->infopage->info_pagina_facebook) { ?>
								<a href="<?php echo $this->infopage->info_pagina_facebook ?>" target="_blank" class="red">
									<i class="fab fa-facebook-f"></i>
								</a>
							<?php } ?>
							<?php if ($this->infopage->info_pagina_twitter) { ?>
								<a href="<?php echo $this->infopage->info_pagina_twitter ?>" target="_blank" class="red">
									<i class="fab fa-twitter"></i>
								</a>
							<?php } ?>
							<?php if ($this->infopage->info_pagina_instagram) { ?>
								<a href="<?php echo $this->infopage->info_pagina_instagram ?>" target="_blank" class="red">
									<i class="fab fa-instagram"></i>
								</a>
							<?php } ?>
							<?php if ($this->infopage->info_pagina_youtube) { ?>
								<a href="<?php echo $this->infopage->info_pagina_youtube ?>" target="_blank" class="red">
									<i class="fab fa-youtube"></i>
								</a>
							<?php } ?>
							<?php if ($this->infopage->info_pagina_pinterest) { ?>
								<a href="<?php echo $this->infopage->info_pagina_pinterest ?>" target="_blank" class="red">
									<i class="fab fa-pinterest-p"></i>
								</a>
							<?php } ?>
							<?php if ($this->infopage->info_pagina_linkedin) { ?>
								<a href="<?php echo $this->infopage->info_pagina_linkedin ?>" target="_blank" class="red">
									<i class="fab fa-linkedin-in"></i>
								</a>
							<?php } ?>
							<?php if ($this->infopage->info_pagina_google) { ?>
								<a href="<?php echo $this->infopage->info_pagina_google ?>" target="_blank" class="red">
									<i class="fab fa-google-plus-g"></i>
								</a>
							<?php } ?>
							<?php if ($this->infopage->info_pagina_flickr) { ?>
								<a href="<?php echo $this->infopage->info_pagina_flickr ?>" target="_blank" class="red">
									<i class="fab fa-flickr"></i>
								</a>
							<?php } ?>
							<div>
								<?php if ($this->infopage->info_pagina_whatsapp) { ?>
									<?php $whatsapp = intval(preg_replace('/[^0-9]+/', '', $this->infopage->info_pagina_whatsapp), 10);  ?>
									<a href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp; ?>" target="_blank" class="red">
										<i class="fab fa-whatsapp"></i>
										<span><?php echo $this->infopage->info_pagina_whatsapp ?></span>
									</a>
								<?php } ?>
							</div>
							<div class="d-none">
								<a href="" target="_blank" class="red">
									<i class="far fa-envelope"></i>
									<span><?php echo $this->infopage->info_pagina_correos_contacto; ?></span>
								</a>
							</div>
						</div>

					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="pqr-app">
	<?php foreach ($this->botones as $key => $boton) { ?>
		<div class="app" style="background:<?php echo $boton->publicidad_color_fondo ?>" align="center" >
			<a href="<?php echo $boton->publicidad_enlace ?>" target="blank">
				<p> <?php echo $boton->publicidad_nombre ?></p>
				<p align="center"><img src="/images/<?php echo $boton->publicidad_imagen ?>" alt=""> </p>
			</a>
		</div>
	<?php } ?>
    
</div>
<script>
	function mostrar() {
		var nosotros = document.getElementById('nosotros');
		if (nosotros.style.display != "block") {
			document.getElementById('nosotros').style.display = "block";
		} else {
			document.getElementById('nosotros').style.display = "none";

		}
	}
</script>