
<div class="titulo-internas" style="background-image:url(/skins/page/images/LogoLigadePatinaje.png)">
    <div align="center"><h2>Disciplinas</h2></div>
</div>
<div class="convenios">
    <div class="container">
        <div class="row">
            <?php foreach ($this->categorias as $key => $convenios) { ?>
              <div class="col-sm-12 col-md-6 col-lg-4 convenio-interna">
                <div class="seccion-convenios">
                  <div><img style="height: 200px;max-height: 200px;" src="/images/<?php echo $convenios["detalle"]->disciplinas_categoria_imagen; ?>" alt=""></div>
                  <h2><a ><?php echo $convenios["detalle"]->disciplinas_categoria_nombre; ?></a></h2>
                 <?php foreach ($convenios["disciplinas"] as $key2 => $convenio) { ?>
                  <div class="lista"><a data-toggle="modal" data-target="#convenio<?php echo $convenio->disciplinas_id?>"><?php echo $convenio->disciplinas_nombre; ?></a></div>
                  <div class="modal fade" id="convenio<?php echo $convenio->disciplinas_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document" style="width: 751px;max-width: 750px;">
                      <div class="modal-content">
                        <div class="modal-header">
                       
                          <h5 class="modal-title" id="exampleModalLabel"><i style="padding-left: 13px;padding-right: 6px;" class="fas fa-swimmer"></i><?php echo $convenio->disciplinas_nombre; ?></h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="col-sm-12"><div class="imagen-modal"><img style="width:50%;" src="/images/<?php echo $convenio->disciplinas_imagen; ?>" alt=""></div></div>
                          <div class="col-sm-12"><?php echo $convenio->disciplinas_descripcion ?></div>
                          <?php if ($convenio->disciplinas_archivo) { ?>
                            <div align="center" class="col-sm-12"><a style="text-decoration:none; color:#77A53D" target="blank" href="/files/<?php echo $convenio->disciplinas_archivo ?>"><?php echo $convenio->disciplinas_nombrearchivo ?> <i class="fas fa-download"></i></a></div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php } ?>
                </div> 
              </div>
            <?php } ?>
        </div> 
    </div> 
</div>