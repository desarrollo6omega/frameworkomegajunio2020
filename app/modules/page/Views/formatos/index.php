<div class="titulo-internas" style="background-image:url(/skins/page/images/LogoLigadePatinaje.png)">
    <div align="center">
        <h2>Comunicados y Resoluciones</h2>
    </div>
</div>
<div class="padding-contenedor productos">
    <div align="center">
        <div class="container">

            <div class="row">
                <div class="col-lg-3">
                    <form class="caja-buscador">
                        <div class="form-group">
                            <input type="text" class="form-control buscador" id="buscador" onkeyup="myFunction()" name="buscador" placeholder="Buscar Formato">
                        </div>
                    </form>
                    <div class="lateral">
                        <h2 class="text-center title_filtro">Categorías de Formatos <i class="fas fa-caret-down"></i></h2>


                        <div class="list_categoria">
                            <a class="filtro_documentos " href="/page/formatos">&nbsp;<i class="far fa-file-alt"></i> Todos los Formatos</a>

                            <a class="filtro_documentos <?php if ($_GET['categoria'] == '1') { ?>active <?php } ?>" href="/page/formatos?categoria=1">&nbsp;<i class="fas fa-quidditch"></i></i> Liga</a>


                            <a class="filtro_documentos <?php if ($_GET['categoria'] == '2') { ?>active <?php } ?>" href="/page/formatos?categoria=2">&nbsp;<i class="fas fa-flag-checkered"></i> Carreras</a>


                            <a class="filtro_documentos <?php if ($_GET['categoria'] == '3') { ?>active <?php } ?>" href="/page/formatos?categoria=3">&nbsp;<i class="fab fa-slideshare"></i> Artístico</a>


                            <a class="filtro_documentos <?php if ($_GET['categoria'] == '4') { ?>active <?php } ?>" href="/page/formatos?categoria=4">&nbsp;<i class="fas fa-hockey-puck"></i> Hockey Línea</a>


                            <a class="filtro_documentos <?php if ($_GET['categoria'] == '5') { ?>active <?php } ?>" href="/page/formatos?categoria=5">&nbsp;<i class="fas fa-quidditch"></i></i> Hockey Patín</a>


                        </div>
                    </div>
                </div>

                <div class="col-lg-9 formatos">
                    <ul class="titulo-formatos">
                        <li>
                            <h4>Comunicados</h4>
                        </li>
                    </ul>
                    <div id="<?php echo $this->cat ?>" class="ancla">
                        <?php foreach ($this->productos as $key => $producto) : ?>
                            <?php if ($producto->formato_seccion == '1') { ?>
                                <ul>
                                    <li>

                                        <a href="/page/productos/detalle?id=<?php echo $producto->formato_id ?>&categoria=<?php echo $producto->formato_subcategoria; ?>">

                                        </a>
                                        <a href="/files/<?php echo $producto->formato_archivo ?>" target="blank"><?php if ($producto->formato_archivo) { ?>
                                                <i class="fas fa-file-download"></i><?php } ?> </a>
                                        <div class="producto-name align-self-start" style="word-break:break-word;">
                                            <h6><?php echo $producto->formato_nombre; ?></h6>
                                        </div>
                                    </li>
                                </ul><?php } ?>

                        <?php endforeach ?>
                    </div>
                    <ul class="titulo-formatos">
                        <li>
                            <h4>Resoluciones</h4>
                        </li>
                    </ul>
                    <div id="<?php echo $this->cat ?>" class="ancla">
                        <?php foreach ($this->productos as $key => $producto) : ?>
                            <?php if ($producto->formato_seccion == '2') { ?>
                                <ul>
                                    <li>

                                        <a href="/page/productos/detalle?id=<?php echo $producto->formato_id ?>&categoria=<?php echo $producto->formato_subcategoria; ?>">

                                        </a>
                                        <a href="/files/<?php echo $producto->formato_archivo ?>" target="blank"><?php if ($producto->formato_archivo) { ?>
                                                <i class="fas fa-file-download"></i><?php } ?> </a>
                                        <div class="producto-name align-self-start" style="word-break:break-word;">
                                            <h6><?php echo $producto->formato_nombre; ?></h6>
                                        </div>
                                    </li>
                                </ul><?php } ?>

                        <?php endforeach ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>