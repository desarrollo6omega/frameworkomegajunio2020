<div class="titulo-internas" style="background-image:url(/skins/page/images/LogoLigadePatinaje.png)">
    <div align="center"><h2>Recordar Contraseña</h2></div>
</div>
<div class="olvido">
    <div class="container">
        <div class="text-center"> 
            Por favor ingrese su dirección de correo electrónico y 
            recibirás un enlace para crear una nueva contraseña.
        </div>
        <br>
        <form  autocomplete="off" action="/page/login/forgotpassword2" method="post" >
            <div class="form-group " >
                <label class="control-label sr-only">Correo</label>
                <div class="input-group">
                    <i class="fas fa-envelope icon-input-left"></i>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Correo" required>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <?php if($this->error_olvido){ ?>
                <div class="alert alert-danger" align="center"><?php echo $this->error_olvido; ?></div>
            <?php } ?>
            <?php if($this->mensaje_olvido){ ?>
                <div class="alert alert-success" align="center"><?php echo $this->mensaje_olvido; ?></div>
            <?php } ?>
            <input type="hidden" id="csrf" name="csrf" value="<?php echo $this->csrf; ?>" />
            <div class="text-center volver"><a style="cursor:pointer;" data-toggle="modal" data-target="#exampleModal">Volver al Login</a></div>
            <div class="text-center"><button  class="btn btn-primary" type="submit">Enviar</button></div>
        </form>
    </div>
</div>