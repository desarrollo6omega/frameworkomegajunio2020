<?php echo $this->bannerprincipal; ?>
<div class="fondo-azul">

</div>
<?php echo $this->contenidohome; ?>
<div class="galeria-album d-none">
    <div class="container">
        <div class="carrusel-album">
            <div class=container>
                <div class="col-12">
                    <div id='carousel_container'>
                        <div class='left_scroll'><i class="fas fa-chevron-left"></i></div>
                        <div class="carousel_inner">
                            <ul>
                                <?php foreach ($this->album as $key => $album) { ?>
                                    <li>
                                        <div class="carrusel-detalle">
                                            <div class="album-index">
                                                <a href="/page/eventos/detalle?id=<?php echo $album->album_id; ?>">
                                                    <div class="imagen-album" align="center"><img src="/images/<?php echo $album->album_imagen ?>" alt=""></div>
                                                    <div class="titulo">
                                                        <h3><?php echo $album->album_titulo; ?></h3>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class='right_scroll'><i class="fas fa-chevron-right"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->noticiastitulo; ?>
<div class="noticias">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-8">
                <div class="row">
                    <?php foreach ($this->noticias as $key => $noticia) { ?>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="padding-noticias">
                                <div class="noticia">
                                    <a href="/page/noticias/detalle?id=<?php echo $noticia->contenido_id; ?>">
                                        <div align="center"><img src="/images/<?php echo $noticia->contenido_imagen ?>" alt=""></div>
                                    </a>
                                    <div class="titulo">
                                        <h4><?php echo $noticia->contenido_titulo ?></h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="fecha"><?php echo $noticia->contenido_fecha ?></div>
                                        </div>
                                        <div class="col-7">
                                            <div class="compartir" align="right">Compartir:
                                                <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//<?php echo $_SERVER['HTTP_HOST'] ?>/page/noticias/detalle?id=<?php echo $album->album_id ?>" target="blank"><i class="fab fa-facebook-f"></i></a>
                                                <a href="whatsapp://send?text=<?php echo $_SERVER['HTTP_HOST'] ?>/page/noticias/detalle?id=<?php echo $album->album_id ?>" target="blank"><i class="fab fa-whatsapp"></i></a>
                                                <a href="https://twitter.com/home?status=http%3A//<?php echo $_SERVER['HTTP_HOST'] ?>/page/noticias/detalle?id=<?php echo $album->album_id ?>" target="blank"><i class="fab fa-twitter"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-4">
                <div class="facebook">
                    <div id="fb-root"></div>
                    <script>
                        (function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2';
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    </script>
                    <div class="fb-page" data-href="https://www.facebook.com/Liga-De-Patinaje-De-Bogot%C3%A1-Oficial-325908371305372/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/Liga-De-Patinaje-De-Bogot%C3%A1-Oficial-325908371305372/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Liga-De-Patinaje-De-Bogot%C3%A1-Oficial-325908371305372/">Liga de Patinaje de Bogotá</a></blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        $("#carousel_container").carousel({
            pause: 5000,
            quantity: 3,
            auto: 'true',
            sizes: {
                '960': 2,
                '768': 2,
                '576': 1,
            }
        });
    });
</script>

<!--<?php echo $this->clasificados; ?>-->
<div class="imagenes d-none">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div align="center"><img src="/skins/page/images/supersolidaria.png" alt=""></div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="coop" align="center"><img src="/skins/page/images/coop.png" alt=""></div>
            </div>
        </div>
    </div>
</div>
<?php if ($this->modal->publicidad_imagen != "") { ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12"><a href="<?php echo $this->modal->publicidad_enlace; ?>" target="<?php echo $this->modal->publicidad_tipo_enlace; ?>">
                            <div class="imagen-modal1"><img src="/images/<?php echo $this->modal->publicidad_imagen; ?>" alt=""></div>
                        </a></div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<script>
    $(document).ready(function() {
        $('#myModal').modal('show')
    });
</script>