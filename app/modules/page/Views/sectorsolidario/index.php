<div class="titulo-internas" style="background-image:url(/skins/page/images/LogoLigadePatinaje.png)">
    <div align="center"><h2>Noticias Sector Solidario</h2></div>
</div>
<div class="noticias-interna">
    <div class="noticias">
        <div class="container">
            <div class="row">
                <?php foreach ($this->jeunet as $key => $noticia) { ?>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="padding-noticias">
                            <div class="noticia">
                                <a href="/page/noticias/detalle?id=<?php echo $noticia->contenido_id; ?>">
                                    <div align="center"><img src="/images/<?php echo $noticia->contenido_imagen ?>" alt=""></div>
                                </a>
                                <div class="titulo"><h4><?php echo $noticia->contenido_titulo ?></h4></div>
                                <div class="boton-crediciti" style="text-align: center;padding-bottom: 10px;">
                                    <?php if($noticia->contenido_enlace != ""){ ?>
                                        <?php if($noticia->contenido_vermas != ""){ ?>
                                        
                                    <a href="<?php echo $noticia->contenido_enlace; ?>" class="btn btn-vermas" style="background: #77A53D;border: none;padding-top: 10px;padding-left: 10px;padding-right: 10px; -webkit-border-radius: 15px 15px 15px 15px;border-radius: 20px 20px 20px 19px;font-size: 12px;font-family: 'AvenirLTStd-Light';"><?php echo $noticia->contenido_vermas; ?></a>
                                    <?php } ?>
                                    <?php } ?>
    
                                </div>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="fecha"><?php echo $noticia->contenido_fecha ?></div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="compartir" align="right" >Compartir:
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//<?php echo $_SERVER['HTTP_HOST'] ?>/page/jeunet/detalle?id=<?php echo $noticia->contenido_id ?>"  target="blank"><i class="fab fa-facebook-f"></i></a>
                                            <a href="whatsapp://send?text=<?php echo $_SERVER['HTTP_HOST'] ?>/page/jeunet/detalle?id=<?php echo $noticia->contenido_id ?>"  target="blank"><i class="fab fa-whatsapp"></i></a>
                                            <a href="https://twitter.com/home?status=http%3A//<?php echo $_SERVER['HTTP_HOST'] ?>/page/jeunet/detalle?id=<?php echo $noticia->contenido_id ?>"  target="blank"><i class="fab fa-twitter"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="container">
	<div align="center">
		<ul class="pagination justify-content-center">
			<?php
				$url = '/page/jeunet';
				if ($this->totalpages > 1) {
					if ($this->page != 1)
						echo '<li class="page-item"><a class="page-link" href="'.$url.'?page='.($this->page-1).'"> &laquo; Anterior </a></li>';
					for ($i=1;$i<=$this->totalpages;$i++) {
						if ($this->page == $i)
							echo '<li class="page-item active"><a class="page-link">'.$this->page.'</a></li>';
						else
							echo '<li class="page-item"><a class="page-link" href="'.$url.'?page='.$i.'">'.$i.'</a></li>  ';
					}
					if ($this->page != $this->totalpages)
						echo '<li class="page-item"><a class="page-link" href="'.$url.'?page='.($this->page+1).'">Siguiente &raquo;</a></li>';
				}
			?>
		</ul>
	</div>
</div>
<style>
    .crediciti .boton-crediciti .btn-vermas {
    background: #77A53D!important;
    border: none;
    /* padding: 5px; */
    padding-top: 10px!important;
    padding-left: 10px!important;
    padding-right: 10px!important;
    -webkit-border-radius: 15px 15px 15px 15px!important;
    border-radius: 20px 20px 20px 19px!important;
    font-size: 12px!important;
    font-family: 'AvenirLTStd-Light'!important;
}
.crediciti .boton-crediciti {
    text-align: center;
    padding-bottom: 10px;
}
</style>