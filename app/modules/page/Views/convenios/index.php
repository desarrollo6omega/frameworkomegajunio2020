<div class="titulo-internas" style="background-image:url(/skins/page/images/LogoLigadePatinaje.png)">
  <div align="center">
    <h2>Convenios</h2>
  </div>
</div>
<div class="convenios">
  <div class="container">
    <div class="row">
      <?php foreach ($this->categorias as $key => $convenios) { ?>
        <div class="col-sm-12 col-md-6 col-lg-4 convenio-interna">
          <div class="seccion-convenios">
            <div><img src="/images/<?php echo $convenios["detalle"]->convenios_categoria_imagen; ?>" alt=""></div>
            <h2><a><?php echo $convenios["detalle"]->convenios_categoria_nombre; ?></a></h2>
            <?php foreach ($convenios["convenios"] as $key2 => $convenio) { ?>
              <div class="lista"><a data-toggle="modal" data-target="#convenio<?php echo $convenio->convenios_id ?>"><?php echo $convenio->convenios_nombre ?></a></div>
              <div class="modal fade" id="convenio<?php echo $convenio->convenios_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document" style="width: 751px;max-width: 750px;">
                  <div class="modal-content">
                    <div class="modal-header">

                      <h5 class="modal-title" id="exampleModalLabel"><i style="padding-left: 13px;padding-right: 6px;" class="far fa-handshake"></i><?php echo $convenio->convenios_nombre; ?></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="col-sm-12">
                        <div class="imagen-modal"><img src="/images/<?php echo $convenio->convenios_imagen; ?>" alt=""></div>
                      </div>
                      <div class="col-sm-12"><?php echo $convenio->convenios_descripcion ?></div>
                      <?php if ($convenio->convenios_archivo) { ?>
                        <div align="center" class="col-sm-12"><a style="text-decoration:none; color:#77A53D" target="blank" href="/files/<?php echo $convenio->convenios_archivo ?>"><?php echo $convenio->convenios_nombrearchivo ?> <i class="fas fa-download"></i></a></div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>