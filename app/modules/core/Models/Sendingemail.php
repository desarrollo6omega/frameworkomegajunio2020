<?php

/**
* Modelo del modulo Core que se encarga de  enviar todos los correos nesesarios del sistema.
*/
class Core_Model_Sendingemail
{
	/**
     * Intancia de la calse emmail
     * @var class
     */
    protected $email;

    protected $_view;

     public function __construct($view)
    {
        $this->email = new Core_Model_Mail();
        $this->_view = $view;
    }


    public function forgotpassword($user)
    {
        if ($user) {
            $code = [];
            $code['user'] = $user->user_id;
            $code['code'] = $user->code;
            $codeEmail = base64_encode(json_encode($code));
            $this->_view->url = "http://".$_SERVER['HTTP_HOST']."/administracion/index/changepassword?code=".$codeEmail; 
           	$this->_view->host = "http://".$_SERVER['HTTP_HOST']."/";
            $this->_view->nombre = $user->user_names." ".$user->user_lastnames;
            $this->_view->usuario = $user->user_user;
            /*fin parametros de la vista */
            //$this->email->getMail()->setFrom("desarrollo4@omegawebsystems.com","Intranet Coopcafam");
            $this->email->getMail()->addAddress($user->user_email,  $user->user_names." ".$user->user_lastnames);
            $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/forgotpassword.php');
            $this->email->getMail()->Subject = "Recuperación de Contraseña Gestor de Contenidos";
            $this->email->getMail()->msgHTML($content);
            $this->email->getMail()->AltBody = $content;
            if ($this->email->sed()==true) {
                return true;
            } else {
                return false; 
            }
        }
    }
     public function forgotpassword2($user)
    {
        if ($user) {
            $code = [];
            $code['user'] = $user->user_id;
            $code['code'] = $user->code;
            $codeEmail = base64_encode(json_encode($code));
            $this->_view->url = "https://".$_SERVER['HTTP_HOST']."/page/index/changepassword?code=".$codeEmail; 
           	$this->_view->host = "https://".$_SERVER['HTTP_HOST']."/";
            $this->_view->nombre = $user->user_names." ".$user->user_lastnames;
            $this->_view->usuario = $user->user_user;
            /*fin parametros de la vista */
            $this->email->getMail()->setFrom("desarrollo5@omegawebsystems.com","Recuperación Contraseña Crediciti");
            $this->email->getMail()->addAddress($user->user_email,  $user->user_names." ".$user->user_lastnames);
            $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/forgotpassword.php');
            $this->email->getMail()->Subject = "Recuperación de Contraseña";
            $this->email->getMail()->msgHTML($content);
            $this->email->getMail()->AltBody = $content;
            if ($this->email->sed()==true) {
                return 1;
            } else {
                return 2; 
            }
        }
    }
    public function enviarcorreo ($data) 
    {
        $this->_view->data = $data;
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $correo = $informacion->info_pagina_correos_contacto;
        $this->email->getMail()->addAddress($correo,  "Contacto Crediciti");
        $this->email->getMail()->setFrom($data['email'], $data['nombre']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/informacion.php');
        $this->email->getMail()->Subject = "Contáctenos Crediciti";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
		$this->email->getMail()->addBCC($informacion->info_pagina_correo_oculto);

        if ($this->email->sed()==true) {
            return 1;
        } else {
            return 2;
        }
    }
    public function enviarcorreo1 ($data) 
    {
        $this->_view->data = $data;
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $correo = $informacion->info_pagina_correos_contacto;
        $this->email->getMail()->addAddress("pqrs@crediciti.coop",  "Contacto Crediciti PQR");
        $this->email->getMail()->setFrom($data['email'] ,$data['nombre']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/informacion2.php');
        $this->email->getMail()->Subject = "Contáctenos Crediciti PQR";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return 1;
        } else {
            return 2;
        }
    }
    public function enviarcorreo2 ($data, $crediagil) 
    {   
        $this->_view->crediagil = $crediagil;
        $this->_view->data = $data;
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $correo = $informacion->info_pagina_correos_contacto;
        $this->email->getMail()->addAddress("crediagil@crediciti.coop", "Solicitud Crediagil");
        $this->email->getMail()->addBCC("desarrollo3@omegawebsystems.com", "Solicitud Crediagil");
        $this->email->getMail()->setFrom($data['email'] ,$data['nombre']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/informacion3.php');
        $this->email->getMail()->Subject = "Solicitud Crediagil";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return 1;
        } else {
            return 2; 
        }
    }
    public function enviarcorreoprueba ($data, $crediagil) 
    {   
        $this->_view->crediagil = $crediagil;
        $this->_view->data = $data;
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $correo = $informacion->info_pagina_correos_contacto;
        //$this->email->getMail()->addAddress("crediagil@crediciti.coop", "Solicitud Crediagil");
        $this->email->getMail()->addAddress("soporteomega@omegawebsystems.com", "Solicitud Crediagil");
        $this->email->getMail()->setFrom($data['email'] ,$data['nombre']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/informacion3.php');
        $this->email->getMail()->Subject = "test Solicitud Crediagil";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        $this->email->getMail()->SMTPDebug = 2;
        if ($this->email->sed()==true) {
            return 1;
        } else {
            return 2; 
        }

    }
    public function confirmacion ($data) 
    {
        $this->_view->data = $data;
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $correo = $informacion->info_pagina_correos_contacto;
        //$this->email->getMail()->addAddress("serviciosasociados@crediciti.coop", "Solicitud De Clasificado");
        $this->email->getMail()->addAddress("comunicaciones@crediciti.coop", "Solicitud De Clasificado");
        $this->email->getMail()->addBCC("desarrollo5@omegawebsystems.com","Solicitud De Clasificado");
        $this->email->getMail()->setFrom($data['email'] ,$data['nombre']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/informacion4.php');
        $this->email->getMail()->Subject = "Solicitud De Clasificado";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return 1;
        } else {
            return 2; 
        }
    }
    public function confirmacion2 ($data) 
    {
        $this->_view->data = $data;
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $contentModel = new Page_Model_DbTable_Eventos();
        $eventos = $contentModel->getList("","");
        $evento = array(); 
        foreach ($eventos as $key => $value) {
            $evento[$value->evento_id] = $value->evento_nombre;
        }
        $this->_view->evento = $evento; 
        $correo = $informacion->info_pagina_correos_contacto;
        $this->email->getMail()->addAddress("inscripciones@crediciti.coop","Solicitud De Inscripcion a Evento");
        $this->email->getMail()->addBCC("desarrollo3@omegawebsystems.com","Solicitud De Inscripcion a Evento");
        $this->email->getMail()->addBCC("jlacharme@why.com.co","Solicitud De Inscripcion a Evento");
        $this->email->getMail()->addBCC("comercial@why.com.co","Solicitud De Inscripcion a Evento");
        $this->email->getMail()->setFrom($data['email'] ,$data['nombre']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/informacion5.php');
        $this->email->getMail()->Subject = "Solicitud De Inscripción a Evento";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) { 
            return 1;
        } else {
            return 2; 
        }
    }
    public function confirmacion3 ($data) 
    {
        $this->_view->data = $data;
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $contentModel = new Page_Model_DbTable_Boletaevento();
        $boletas = $contentModel->getList("","");
        $boleta = array();
        foreach ($boletas as $key => $value) {
            $boleta[$value->boletaevento_id] = $value->boletaevento_tipo;
        }
        $this->_view->boleta = $boleta;
        $contentModel = new Page_Model_DbTable_Boleteriaevento();
        $eventos = $contentModel->getList("","");
        $evento = array(); 
        foreach ($eventos as $key => $value) {
            $evento[$value->boleteriaevento_id] = $value->boleteriaevento_nombre;
        }
        $this->_view->evento = $evento; 
        $correo = $informacion->info_pagina_correos_contacto;
        $this->email->getMail()->addAddress("inscripciones@crediciti.coop","Solicitud De Boletería");
        $this->email->getMail()->addBCC("desarrollo3@omegawebsystems.com","Solicitud De Boletería");
        $this->email->getMail()->setFrom($data['email'] ,$data['nombre']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/informacion6.php');
        $this->email->getMail()->Subject = "Solicitud De Boletería";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return 1;
        } else {
            return 2; 
        }
    }
    public function confirmacion4 ($data) 
    {
        $this->_view->data = $data;
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $correo = $informacion->info_pagina_correos_contacto;
        $this->email->getMail()->addAddress("servicioasociados@crediciti.coop","Solicitud De Modificación de Aportes");
        $this->email->getMail()->addBCC("desarrollo3@omegawebsystems.com","Solicitud De Modificación de Aportes");
        $this->email->getMail()->setFrom($data['email'] ,$data['nombre']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/informacion7.php');
        $this->email->getMail()->Subject = "Solicitud De Modificación de Aportes";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return 1;
        } else {
            return 2; 
        }
    }
    public function enviarcorreousuario ($data) 
    {
        $this->_view->data = $data;
        $this->email->getMail()->addAddress($data['inscripciones_correo'] ,$data['inscripcion_nombre']);
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $correo = $informacion->info_pagina_correos_contacto;
        $this->email->getMail()->setFrom($correo ,$data['inscripcion_nombre']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/usuario.php');
        $this->email->getMail()->Subject = "Confirmación de Solicitud de Evento";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return 1;
        } else {
            return 2;
        }
    }
    public function enviarcorreousuario1 ($data) 
    {
        $this->_view->data = $data; 
        $this->email->getMail()->addAddress($data['inscripcionboleteria_correo'] ,$data['inscripcionboleteria_nombre']);
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $correo = $informacion->info_pagina_correos_contacto;
        $this->email->getMail()->setFrom($correo ,$data['inscripcionboleteria_nombre']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/usuario2.php');
        $this->email->getMail()->Subject = "Confirmación de Solicitud de Boletería";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return 1;
        } else {
            return 2;
        }
    }
    public function enviarcorreousuario2 ($data) 
    {
        $this->_view->data = $data;
        $this->email->getMail()->addAddress($data['clasificados_correo'] ,$data['clasificados_nombreusuario']);
        $informacionModel = new Page_Model_DbTable_Informacion();
        $informacion = $informacionModel->getList("","orden ASC")[0];
        $correo = $informacion->info_pagina_correos_contacto;
        $this->email->getMail()->setFrom($correo ,$data['clasificados_nombreusuario']);
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/usuario3.php');
        $this->email->getMail()->Subject = "Confirmación de Publicación de Clasificado";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return 1;
        } else {
            return 2;
        }
    }

}