<?php 
/**
* clase que genera la clase dependiente  de convenios en la base de datos
*/
class Administracion_Model_DbTable_Dependconvenioscategoria extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'convenios_categoria';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'convenios_categoria_id';

}