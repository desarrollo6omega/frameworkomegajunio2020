<?php

/**
 * clase que genera la insercion y edicion  de convenioscategoria en la base de datos
 */
class Administracion_Model_DbTable_Areasgaleriacategoria extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'area';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'area_id';

	/**
	 * insert recibe la informacion de un convenioscategoria y la inserta en la base de datos
	 * @param  array Array array con la informacion con la cual se va a realizar la insercion en la base de datos
	 * @return integer      identificador del  registro que se inserto
	 */
	public function insert($data)
	{
		$area_titulo = $data['area_titulo'];
		$orden = $data['orden'];

		$query = "INSERT INTO area(area_titulo, orden) VALUES ('$area_titulo','$orden')";
		$res = $this->_conn->query($query);
		return mysqli_insert_id($this->_conn->getConnection());
	}

	/**
	 * update Recibe la informacion de un convenioscategoria  y actualiza la informacion en la base de datos
	 * @param  array Array Array con la informacion con la cual se va a realizar la actualizacion en la base de datos
	 * @param  integer    identificador al cual se le va a realizar la actualizacion
	 * @return void
	 */
	public function update($data, $id)
	{

		$area_titulo = $data['area_titulo'];
		$orden = $data['orden'];
		$query = "UPDATE area SET  area_titulo = '$area_titulo', orden = '$orden' WHERE convenios_categoria_id = '" . $id . "'";
		$res = $this->_conn->query($query);
	}
}
