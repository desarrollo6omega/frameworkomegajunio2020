<?php 
/**
* clase que genera la insercion y edicion  de ciudades de  galeria en la base de datos
*/
class Administracion_Model_DbTable_Ciudadgaleria extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'ciudadgaleria';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'ciudadgaleria_id';

	/**
	 * insert recibe la informacion de un ciudades de galeria y la inserta en la base de datos
	 * @param  array Array array con la informacion con la cual se va a realizar la insercion en la base de datos
	 * @return integer      identificador del  registro que se inserto
	 */
	public function insert($data){
		$ciudadgaleria_titulo = $data['ciudadgaleria_titulo'];
		$query = "INSERT INTO ciudadgaleria( ciudadgaleria_titulo) VALUES ( '$ciudadgaleria_titulo')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	/**
	 * update Recibe la informacion de un ciudades de galeria  y actualiza la informacion en la base de datos
	 * @param  array Array Array con la informacion con la cual se va a realizar la actualizacion en la base de datos
	 * @param  integer    identificador al cual se le va a realizar la actualizacion
	 * @return void
	 */
	public function update($data,$id){
		
		$ciudadgaleria_titulo = $data['ciudadgaleria_titulo'];
		$query = "UPDATE ciudadgaleria SET  ciudadgaleria_titulo = '$ciudadgaleria_titulo' WHERE ciudadgaleria_id = '".$id."'";
		$res = $this->_conn->query($query);
	}
}