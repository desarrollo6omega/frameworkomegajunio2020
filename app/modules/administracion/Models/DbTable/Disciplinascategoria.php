<?php 
/**
* clase que genera la insercion y edicion  de convenioscategoria en la base de datos
*/
class Administracion_Model_DbTable_Disciplinascategoria extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'disciplinas_categoria';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'disciplinas_categoria_id';

	/**
	 * insert recibe la informacion de un convenioscategoria y la inserta en la base de datos
	 * @param  array Array array con la informacion con la cual se va a realizar la insercion en la base de datos
	 * @return integer      identificador del  registro que se inserto
	 */
	public function insert($data){
		$disciplinas_categoria_nombre = $data['disciplinas_categoria_nombre'];
		$disciplinas_categoria_imagen = $data['disciplinas_categoria_imagen'];
		$disciplinas_categoria_descripcion = $data['disciplinas_categoria_descripcion'];
		$disciplinas_categoria_estado = $data['disciplinas_categoria_estado'];
		$query = "INSERT INTO disciplinas_categoria( disciplinas_categoria_nombre, disciplinas_categoria_imagen, disciplinas_categoria_descripcion, disciplinas_categoria_estado) VALUES ( '$disciplinas_categoria_nombre', '$disciplinas_categoria_imagen', '$disciplinas_categoria_descripcion', '$disciplinas_categoria_estado')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	/**
	 * update Recibe la informacion de un convenioscategoria  y actualiza la informacion en la base de datos
	 * @param  array Array Array con la informacion con la cual se va a realizar la actualizacion en la base de datos
	 * @param  integer    identificador al cual se le va a realizar la actualizacion
	 * @return void
	 */
	public function update($data,$id){
		
		$disciplinas_categoria_nombre = $data['disciplinas_categoria_nombre'];
		$disciplinas_categoria_imagen = $data['disciplinas_categoria_imagen'];
		$disciplinas_categoria_descripcion = $data['disciplinas_categoria_descripcion'];
		$disciplinas_categoria_estado = $data['disciplinas_categoria_estado'];
		$query = "UPDATE disciplinas_categoria SET  disciplinas_categoria_nombre = '$disciplinas_categoria_nombre', disciplinas_categoria_imagen = '$disciplinas_categoria_imagen', disciplinas_categoria_descripcion = '$disciplinas_categoria_descripcion', disciplinas_categoria_estado = '$disciplinas_categoria_estado' WHERE disciplinas_categoria_id = '".$id."'";
		$res = $this->_conn->query($query);
	}
}