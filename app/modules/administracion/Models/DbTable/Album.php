<?php 
/**
* clase que genera la insercion y edicion  de album en la base de datos
*/
class Administracion_Model_DbTable_Album extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'album';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'album_id';

	/**
	 * insert recibe la informacion de un album y la inserta en la base de datos
	 * @param  array Array array con la informacion con la cual se va a realizar la insercion en la base de datos
	 * @return integer      identificador del  registro que se inserto
	 */
	public function insert($data){
		$album_titulo = $data['album_titulo'];
		$album_descripcion = $data['album_descripcion'];
		$album_imagen = $data['album_imagen'];
		$album_fecha = $data['album_fecha'];
		$area = $data['area'];
		$ciudad = $data['ciudad'];
		$query = "INSERT INTO album( album_titulo, album_descripcion, album_imagen, album_fecha, area_id, ciudad_id) VALUES ( '$album_titulo', '$album_descripcion', '$album_imagen', '$album_fecha', '$area', '$ciudad')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	/**
	 * update Recibe la informacion de un album  y actualiza la informacion en la base de datos
	 * @param  array Array Array con la informacion con la cual se va a realizar la actualizacion en la base de datos
	 * @param  integer    identificador al cual se le va a realizar la actualizacion
	 * @return void
	 */
	public function update($data,$id){
		
		$album_titulo = $data['album_titulo'];
		$album_descripcion = $data['album_descripcion'];
		$album_imagen = $data['album_imagen'];
		$album_fecha = $data['album_fecha'];
		$area = $data['area'];
		$ciudad = $data['ciudad'];
		$query = "UPDATE album SET  album_titulo = '$album_titulo', album_descripcion = '$album_descripcion', album_imagen = '$album_imagen', album_fecha = '$album_fecha', area_id = '$area', ciudad_id = '$ciudad' WHERE album_id = '".$id."'";
		$res = $this->_conn->query($query);
	}
}