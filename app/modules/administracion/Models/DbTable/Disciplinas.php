<?php 
/**
* clase que genera la insercion y edicion  de convenios en la base de datos
*/
class Administracion_Model_DbTable_Disciplinas extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'disciplinas';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'disciplinas_id';

	/**
	 * insert recibe la informacion de un convenios y la inserta en la base de datos
	 * @param  array Array array con la informacion con la cual se va a realizar la insercion en la base de datos
	 * @return integer      identificador del  registro que se inserto
	 */
	public function insert($data){
		$disciplinas_nombre = $data['disciplinas_nombre'];
		$disciplinas_imagen = $data['disciplinas_imagen'];
		$disciplinas_descripcion = $data['disciplinas_descripcion'];
		$disciplinas_categoria = $data['disciplinas_categoria'];
		$disciplinas_nombrearchivo = $data['disciplinas_nombrearchivo'];
		$disciplinas_archivo = $data['disciplinas_archivo'];
		$disciplinas_estado = $data['disciplinas_estado'];
		$query = "INSERT INTO disciplinas( disciplinas_nombre, disciplinas_imagen, disciplinas_descripcion, disciplinas_categoria, disciplinas_nombrearchivo, disciplinas_archivo, disciplinas_estado) VALUES ( '$disciplinas_nombre', '$disciplinas_imagen', '$disciplinas_descripcion', '$disciplinas_categoria', '$disciplinas_nombrearchivo', '$disciplinas_archivo', '$disciplinas_estado')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	/**
	 * update Recibe la informacion de un convenios  y actualiza la informacion en la base de datos
	 * @param  array Array Array con la informacion con la cual se va a realizar la actualizacion en la base de datos
	 * @param  integer    identificador al cual se le va a realizar la actualizacion
	 * @return void
	 */
	public function update($data,$id){
		
		$disciplinas_nombre = $data['disciplinas_nombre'];
		$disciplinas_imagen = $data['disciplinas_imagen'];
		$disciplinas_descripcion = $data['disciplinas_descripcion'];
		$disciplinas_categoria = $data['disciplinas_categoria'];
		$disciplinas_nombrearchivo = $data['disciplinas_nombrearchivo'];
		$disciplinas_archivo = $data['disciplinas_archivo'];
		$disciplinas_estado = $data['disciplinas_estado'];
		$query = "UPDATE disciplinas SET  disciplinas_nombre = '$disciplinas_nombre', disciplinas_imagen = '$disciplinas_imagen', disciplinas_descripcion = '$disciplinas_descripcion', disciplinas_categoria = '$disciplinas_categoria', disciplinas_nombrearchivo = '$disciplinas_nombrearchivo', disciplinas_archivo = '$disciplinas_archivo', disciplinas_estado = '$disciplinas_estado' WHERE disciplinas_id = '".$id."'";
		$res = $this->_conn->query($query);
	}
}