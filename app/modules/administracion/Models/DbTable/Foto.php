<?php 
/**
* clase que genera la insercion y edicion  de foto en la base de datos
*/
class Administracion_Model_DbTable_Foto extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'foto';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'foto_id';

	/**
	 * insert recibe la informacion de un foto y la inserta en la base de datos
	 * @param  array Array array con la informacion con la cual se va a realizar la insercion en la base de datos
	 * @return integer      identificador del  registro que se inserto
	 */
	public function insert($data){
		$foto_titulo = $data['foto_titulo'];
		$foto_descripcion = $data['foto_descripcion'];
		$foto_imagen=str_replace("images/","",$data['foto_imagen']);;
		$album_id = $data['album_id'];
		$query = "INSERT INTO foto( foto_titulo, foto_descripcion, foto_imagen, album_id) VALUES ( '$foto_titulo', '$foto_descripcion', '$foto_imagen', '$album_id')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	/**
	 * update Recibe la informacion de un foto  y actualiza la informacion en la base de datos
	 * @param  array Array Array con la informacion con la cual se va a realizar la actualizacion en la base de datos
	 * @param  integer    identificador al cual se le va a realizar la actualizacion
	 * @return void
	 */
	public function update($data,$id){
		
		$foto_titulo = $data['foto_titulo'];
		$foto_descripcion = $data['foto_descripcion'];
		$foto_imagen=str_replace("images/","",$data['foto_imagen']);;
		$album_id = $data['album_id'];
		$query = "UPDATE foto SET  foto_titulo = '$foto_titulo', foto_descripcion = '$foto_descripcion', foto_imagen = '$foto_imagen', album_id = '$album_id' WHERE foto_id = '".$id."'";
		$res = $this->_conn->query($query);
	}
}