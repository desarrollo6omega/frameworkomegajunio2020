<?php 
/**
* clase que genera la clase dependiente  de inscripcionesboleteria en la base de datos
*/
class Administracion_Model_DbTable_Dependboletaevento extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'boletaevento';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'boletaevento_id';

}