<?php 
/**
* clase que genera la insercion y edicion  de convenios en la base de datos
*/
class Administracion_Model_DbTable_Convenios extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'convenios';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'convenios_id';

	/**
	 * insert recibe la informacion de un convenios y la inserta en la base de datos
	 * @param  array Array array con la informacion con la cual se va a realizar la insercion en la base de datos
	 * @return integer      identificador del  registro que se inserto
	 */
	public function insert($data){
		$convenios_nombre = $data['convenios_nombre'];
		$convenios_imagen = $data['convenios_imagen'];
		$convenios_descripcion = $data['convenios_descripcion'];
		$convenios_categoria = $data['convenios_categoria'];
		$convenios_nombrearchivo = $data['convenios_nombrearchivo'];
		$convenios_archivo = $data['convenios_archivo'];
		$convenios_estado = $data['convenios_estado'];
		$query = "INSERT INTO convenios( convenios_nombre, convenios_imagen, convenios_descripcion, convenios_categoria, convenios_nombrearchivo, convenios_archivo, convenios_estado) VALUES ( '$convenios_nombre', '$convenios_imagen', '$convenios_descripcion', '$convenios_categoria', '$convenios_nombrearchivo', '$convenios_archivo', '$convenios_estado')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	/**
	 * update Recibe la informacion de un convenios  y actualiza la informacion en la base de datos
	 * @param  array Array Array con la informacion con la cual se va a realizar la actualizacion en la base de datos
	 * @param  integer    identificador al cual se le va a realizar la actualizacion
	 * @return void
	 */
	public function update($data,$id){
		
		$convenios_nombre = $data['convenios_nombre'];
		$convenios_imagen = $data['convenios_imagen'];
		$convenios_descripcion = $data['convenios_descripcion'];
		$convenios_categoria = $data['convenios_categoria'];
		$convenios_nombrearchivo = $data['convenios_nombrearchivo'];
		$convenios_archivo = $data['convenios_archivo'];
		$convenios_estado = $data['convenios_estado'];
		$query = "UPDATE convenios SET  convenios_nombre = '$convenios_nombre', convenios_imagen = '$convenios_imagen', convenios_descripcion = '$convenios_descripcion', convenios_categoria = '$convenios_categoria', convenios_nombrearchivo = '$convenios_nombrearchivo', convenios_archivo = '$convenios_archivo', convenios_estado = '$convenios_estado' WHERE convenios_id = '".$id."'";
		$res = $this->_conn->query($query);
	}
}