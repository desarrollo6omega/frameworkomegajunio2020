<?php 
/**
* clase que genera la clase dependiente  de convenios en la base de datos
*/
class Administracion_Model_DbTable_Dependdisciplinascategoria extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'disciplinas_categoria';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'disciplinas_categoria_id';

}