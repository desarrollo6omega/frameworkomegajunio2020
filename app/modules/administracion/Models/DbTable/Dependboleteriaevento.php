<?php 
/**
* clase que genera la clase dependiente  de boletaevento en la base de datos
*/
class Administracion_Model_DbTable_Dependboleteriaevento extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'boleteriaevento';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'boleteriaevento_id';

}