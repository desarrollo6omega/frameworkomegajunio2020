<h1 class="titulo-principal"><i class="fas fa-cogs"></i> <?php echo $this->titlesection; ?></h1>
<div class="container-fluid">
	<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
		<div class="content-dashboard">
			<input type="hidden" name="csrf" id="csrf" value="<?php echo $this->csrf ?>">
			<input type="hidden" name="csrf_section" id="csrf_section" value="<?php echo $this->csrf_section ?>">
			<?php if ($this->content->disciplinas_id) { ?>
				<input type="hidden" name="id" id="id" value="<?= $this->content->disciplinas_id; ?>" />
			<?php }?>
			<div class="row">
				<div class="col-6 form-group">
					<label for="disciplinas_nombre"  class="control-label">Nombre</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-verde " ><i class="fas fa-pencil-alt"></i></span>
						</div>
						<input type="text" value="<?= $this->content->disciplinas_nombre; ?>" name="disciplinas_nombre" id="disciplinas_nombre" class="form-control"   >
					</label>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-12 form-group">
					<label for="disciplinas_imagen" >Imagen</label>
					<input type="file" name="disciplinas_imagen" id="disciplinas_imagen" class="form-control  file-image" data-buttonName="btn-primary" accept="image/gif, image/jpg, image/jpeg, image/png"  >
					<div class="help-block with-errors"></div>
					<?php if($this->content->disciplinas_imagen) { ?>
						<div id="imagen_disciplinas_imagen">
							<img src="/images/<?= $this->content->disciplinas_imagen; ?>"  class="img-thumbnail thumbnail-administrator" />
							<div><button class="btn btn-danger btn-sm" type="button" onclick="eliminarImagen('disciplinas_imagen','<?php echo $this->route."/deleteimage"; ?>')"><i class="glyphicon glyphicon-remove" ></i> Eliminar Imagen</button></div>
						</div>
					<?php } ?>
				</div>
				<div class="col-12 form-group">
					<label for="disciplinas_descripcion" class="form-label" >Descripción</label>
					<textarea name="disciplinas_descripcion" id="disciplinas_descripcion"   class="form-control tinyeditor" rows="10"   ><?= $this->content->disciplinas_descripcion; ?></textarea>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-6 form-group">
					<label class="control-label">Categoría</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-rojo-claro " ><i class="far fa-list-alt"></i></span>
						</div>
						<select class="form-control" name="disciplinas_categoria"   >
							<option value="">Seleccione...</option>
							<?php foreach ($this->list_disciplinas_categoria AS $key => $value ){?>
								<option <?php if($this->getObjectVariable($this->content,"disciplinas_categoria") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
							<?php } ?>
						</select>
					</label>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-6 form-group">
					<label for="disciplinas_nombrearchivo"  class="control-label">Nombre archivo</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-azul " ><i class="fas fa-pencil-alt"></i></span>
						</div>
						<input type="text" value="<?= $this->content->disciplinas_nombrearchivo; ?>" name="disciplinas_nombrearchivo" id="disciplinas_nombrearchivo" class="form-control"   >
					</label>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-6 form-group">
					<label for="disciplinas_archivo" >Archivo</label>
					<input type="file" name="disciplinas_archivo" id="disciplinas_archivo" class="form-control  file-document" data-buttonName="btn-primary" onchange="validardocumento('disciplinas_archivo');" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf" >
					<div class="help-block with-errors"></div>
					<?php if($this->content->disciplinas_archivo) { ?>
						<div id="archivo_disciplinas_archivo">
							<div><?php echo $this->content->disciplinas_archivo; ?></div>
							<div><button class="btn btn-danger btn-sm" type="button" onclick="eliminararchivo('disciplinas_archivo','<?php echo $this->route."/deletearchivo"; ?>')"><i class="glyphicon glyphicon-remove" ></i> Eliminar Archivo</button></div>
						</div>
					<?php } ?>
				</div>
				<div class="col-6 form-group">
					<label class="control-label">Estado</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-azul-claro " ><i class="far fa-list-alt"></i></span>
						</div>
						<select class="form-control" name="disciplinas_estado" id="disciplinas_estado" required>
							<option value="">Seleccione...</option>
							<?php foreach ($this->list_disciplinas_estado AS $key => $value ){?>
								<option <?php if($this->getObjectVariable($this->content,"disciplinas_estado") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
							<?php } ?>
						</select>
					</label>
					<div class="help-block with-errors"></div>
				</div>
			</div>
		</div>
		<div class="botones-acciones">
			<button class="btn btn-guardar" type="submit">Guardar</button>
			<a href="<?php echo $this->route; ?>" class="btn btn-cancelar">Cancelar</a>
		</div>
	</form>
</div>