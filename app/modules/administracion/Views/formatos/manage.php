<h1 class="titulo-principal"><i class="fas fa-cogs"></i> <?php echo $this->titlesection; ?></h1>
<div class="container-fluid">
	<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform; ?>" data-toggle="validator">
		<div class="content-dashboard">
			<input type="hidden" name="csrf" id="csrf" value="<?php echo $this->csrf ?>">
			<input type="hidden" name="csrf_section" id="csrf_section" value="<?php echo $this->csrf_section ?>">
			<?php if ($this->content->formato_id) { ?>
				<input type="hidden" name="id" id="id" value="<?= $this->content->formato_id; ?>" />
			<?php } ?>
			<div class="row">
				<div class="col-6 form-group">
					<label for="formato_nombre" class="control-label">Nombre</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-azul "><i class="fas fa-pencil-alt"></i></span>
						</div>
						<input type="text" value="<?= $this->content->formato_nombre; ?>" name="formato_nombre" id="formato_nombre" class="form-control" required>
					</label>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-6 form-group">
					<label for="formato_archivo">Archivo</label>
					<input type="file" name="formato_archivo" id="formato_archivo" class="form-control  file-document" data-buttonName="btn-primary" onchange="validardocumento('formato_archivo');" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf">
					<div class="help-block with-errors"></div>
					<?php if ($this->content->formato_archivo) { ?>
						<div id="archivo_formato_archivo">
							<div><?php echo $this->content->formato_archivo; ?></div>
							<div><button class="btn btn-danger btn-sm" type="button" onclick="eliminararchivo('formato_archivo','<?php echo $this->route . "/deletearchivo"; ?>')"><i class="glyphicon glyphicon-remove"></i> Eliminar Archivo</button></div>
						</div>
					<?php } ?>
				</div>

				<div class="col-4 form-group">
					<label class="control-label">Sección</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-morado "><i class="far fa-list-alt"></i></span>
						</div>
						<select class="form-control" id="formato_seccion" name="formato_seccion"  required>
							<option value="">Seleccione...</option>
							<?php foreach ($this->list_formato_seccion as $key => $value) { ?>
								<option <?php if ($this->getObjectVariable($this->content, "formato_seccion") == $key) {
											echo "selected";
										} ?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
							<?php } ?>
						</select>
					</label>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-4 form-group sub" id="sub">
					<label class="control-label">Subcategoría</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-morado "><i class="far fa-list-alt"></i></span>
						</div>
						<select class="form-control" name="formato_subcategoria" required>
							<option value="">Seleccione...</option>
							<?php foreach ($this->list_formato_subcategoria as $key => $value) { ?>
								<option <?php if ($this->getObjectVariable($this->content, "formato_subcategoria") == $key) {
											echo "selected";
										} ?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
							<?php } ?>
						</select>
					</label>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-4 form-group">
					<label class="control-label">Estado</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-rosado "><i class="far fa-list-alt"></i></span>
						</div>
						<select class="form-control" name="formato_estado" required>
							<option value="">Seleccione...</option>
							<?php foreach ($this->list_formato_estado as $key => $value) { ?>
								<option <?php if ($this->getObjectVariable($this->content, "formato_estado") == $key) {
											echo "selected";
										} ?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
							<?php } ?>
						</select>
					</label>
					<div class="help-block with-errors"></div>
				</div>
			</div>
		</div>
		<div class="botones-acciones">
			<button class="btn btn-guardar" type="submit">Guardar</button>
			<a href="<?php echo $this->route; ?>" class="btn btn-cancelar">Cancelar</a>
		</div>
	</form>
</div>
<!--<style>
	.sub {
		display: none;
	}
</style>
<script>
	function cate() {
		let seccion = document.getElementById('formato_seccion').value;
		if (seccion == 2) {
			document.getElementById('sub').style.display = 'block';

		} else {
			document.getElementById('sub').style.display = 'none';

		}
	}

	window.onload = cate;
</script>-->