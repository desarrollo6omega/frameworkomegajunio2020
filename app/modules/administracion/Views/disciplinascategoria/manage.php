<h1 class="titulo-principal"><i class="fas fa-cogs"></i> <?php echo $this->titlesection; ?></h1>
<div class="container-fluid">
	<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
		<div class="content-dashboard">
			<input type="hidden" name="csrf" id="csrf" value="<?php echo $this->csrf ?>">
			<input type="hidden" name="csrf_section" id="csrf_section" value="<?php echo $this->csrf_section ?>">
			<?php if ($this->content->disciplinas_categoria_id) { ?>
				<input type="hidden" name="id" id="id" value="<?= $this->content->disciplinas_categoria_id; ?>" />
			<?php }?>
			<div class="row">
				<div class="col-12 form-group">
					<label for="disciplinas_categoria_nombre"  class="control-label">Nombre</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-azul " ><i class="fas fa-pencil-alt"></i></span>
						</div>
						<input type="text" value="<?= $this->content->disciplinas_categoria_nombre; ?>" name="disciplinas_categoria_nombre" id="disciplinas_categoria_nombre" class="form-control"  required >
					</label>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-12 form-group">
					<label for="disciplinas_categoria_imagen" >Imagen</label>
					<input type="file" name="disciplinas_categoria_imagen" id="disciplinas_categoria_imagen" class="form-control  file-image" data-buttonName="btn-primary" accept="image/gif, image/jpg, image/jpeg, image/png"  >
					<div class="help-block with-errors"></div>
					<?php if($this->content->disciplinas_categoria_imagen) { ?>
						<div id="imagen_disciplinas_categoria_imagen">
							<img src="/images/<?= $this->content->disciplinas_categoria_imagen; ?>"  class="img-thumbnail thumbnail-administrator" />
							<div><button class="btn btn-danger btn-sm" type="button" onclick="eliminarImagen('disciplinas_categoria_imagen','<?php echo $this->route."/deleteimage"; ?>')"><i class="glyphicon glyphicon-remove" ></i> Eliminar Imagen</button></div>
						</div>
					<?php } ?>
				</div>
				<div class="col-12 form-group">
					<label for="disciplinas_categoria_descripcion" class="form-label" >Descripción</label>
					<textarea name="disciplinas_categoria_descripcion" id="disciplinas_categoria_descripcion"   class="form-control tinyeditor" rows="10"   ><?= $this->content->disciplinas_categoria_descripcion; ?></textarea>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-12 form-group">
					<label class="control-label">Estado</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-verde " ><i class="far fa-list-alt"></i></span>
						</div>
						<select class="form-control" name="disciplinas_categoria_estado"  required >
							<option value="">Seleccione...</option>
							<?php foreach ($this->list_disciplinas_categoria_estado AS $key => $value ){?>
								<option <?php if($this->getObjectVariable($this->content,"disciplinas_categoria_estado") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
							<?php } ?>
						</select>
					</label>
					<div class="help-block with-errors"></div>
				</div>
			</div>
		</div>
		<div class="botones-acciones">
			<button class="btn btn-guardar" type="submit">Guardar</button>
			<a href="<?php echo $this->route; ?>" class="btn btn-cancelar">Cancelar</a>
		</div>
	</form>
</div>