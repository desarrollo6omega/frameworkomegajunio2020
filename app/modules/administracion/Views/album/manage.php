<h1 class="titulo-principal"><i class="fas fa-cogs"></i> <?php echo $this->titlesection; ?></h1>
<div class="container-fluid">
	<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform; ?>" data-toggle="validator">
		<div class="content-dashboard">
			<input type="hidden" name="csrf" id="csrf" value="<?php echo $this->csrf ?>">
			<input type="hidden" name="csrf_section" id="csrf_section" value="<?php echo $this->csrf_section ?>">
			<?php if ($this->content->album_id) { ?>
				<input type="hidden" name="id" id="id" value="<?= $this->content->album_id; ?>" />
			<?php } ?>
			<div class="row">
				<div class="col-6 form-group">
					<label class="control-label">Ciudad</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-verde "><i class="fa fa-archive"></i></span>
						</div>
						<select class="form-control" name="ciudad" required>
							<option value="">Seleccione...</option>
							<?php foreach ($this->ciudades as $key => $ciudad) { ?>
								<option <?php if ($this->getObjectVariable($this->content, "ciudad_id") == $ciudad->ciudadgaleria_id) {
											echo "selected";
										} ?> value="<?php echo $ciudad->ciudadgaleria_id; ?>" /> <?= $ciudad->ciudadgaleria_titulo; ?>
								</option>
							<?php } ?>
						</select>
					</label>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-6 form-group">
					<label class="control-label">Área</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-azul "><i class="far fa-list-alt"></i></span>
						</div>
						<select class="form-control" name="area" required>
							<option value="">Seleccione...</option>
							<?php foreach ($this->area as $key => $area) { ?>
								<option <?php if ($this->getObjectVariable($this->content, "area_id") == $area->area_id) {
											echo "selected";
										} ?> value="<?php echo $area->area_id; ?>" /> <?= $area->area_titulo; ?>
								</option>
							<?php } ?>
						</select>
					</label>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-12 form-group">
					<label for="album_titulo" class="control-label">Título</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-cafe "><i class="fas fa-pencil-alt"></i></span>
						</div>
						<input type="text" value="<?= $this->content->album_titulo; ?>" name="album_titulo" id="album_titulo" class="form-control" required>
					</label>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-12 form-group">
					<label for="album_descripcion" class="form-label">Descripción</label>
					<textarea name="album_descripcion" id="album_descripcion" class="form-control tinyeditor" rows="10"><?= $this->content->album_descripcion; ?></textarea>
					<div class="help-block with-errors"></div>
				</div>
				<div class="col-12 form-group">
					<label for="album_imagen">Imagen</label>
					<input type="file" name="album_imagen" id="album_imagen" class="form-control  file-image" data-buttonName="btn-primary" accept="image/gif, image/jpg, image/jpeg, image/png" <?php if (!$this->content->album_id) {
																																																		echo 'required';
																																																	} ?>>
					<div class="help-block with-errors"></div>
					<?php if ($this->content->album_imagen) { ?>
						<div id="imagen_album_imagen">
							<img src="/images/<?= $this->content->album_imagen; ?>" class="img-thumbnail thumbnail-administrator" />
							<div><button class="btn btn-danger btn-sm" type="button" onclick="eliminarImagen('album_imagen','<?php echo $this->route . "/deleteimage"; ?>')"><i class="glyphicon glyphicon-remove"></i> Eliminar Imagen</button></div>
						</div>
					<?php } ?>
				</div>

				<div class="col-12 form-group">
					<label for="album_fecha" class="control-label">Fecha</label>
					<label class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text input-icono  fondo-rojo-claro "><i class="fas fa-calendar-alt"></i></span>
						</div>
						<input type="text" value="<?php if ($this->content->album_fecha) {
														echo $this->content->album_fecha;
													} else {
														echo date('Y-m-d');
													} ?>" name="album_fecha" id="album_fecha" class="form-control" required data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-language="es">
					</label>
					<div class="help-block with-errors"></div>
				</div>
			</div>
		</div>
		<div class="botones-acciones">
			<button class="btn btn-guardar" type="submit">Guardar</button>
			<a href="<?php echo $this->route; ?>" class="btn btn-cancelar">Cancelar</a>
		</div>
	</form>
</div>
<style>
	fondo-verde {
		background-color: green;

	}
</style>