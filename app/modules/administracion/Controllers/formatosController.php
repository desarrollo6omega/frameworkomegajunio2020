<?php
/**
* Controlador de Formatos que permite la  creacion, edicion  y eliminacion de los formatos del Sistema
*/
class Administracion_formatosController extends Administracion_mainController
{
	/**
	 * $mainModel  instancia del modelo de  base de datos formatos
	 * @var modeloContenidos
	 */
	public $mainModel;

	/**
	 * $route  url del controlador base
	 * @var string
	 */
	protected $route;

	/**
	 * $pages cantidad de registros a mostrar por pagina]
	 * @var integer
	 */
	protected $pages ;

	/**
	 * $namefilter nombre de la variable a la fual se le van a guardar los filtros
	 * @var string
	 */
	protected $namefilter;

	/**
	 * $_csrf_section  nombre de la variable general csrf  que se va a almacenar en la session
	 * @var string
	 */
	protected $_csrf_section = "administracion_formatos";

	/**
	 * $namepages nombre de la pvariable en la cual se va a guardar  el numero de seccion en la paginacion del controlador
	 * @var string
	 */
	protected $namepages;



	/**
     * Inicializa las variables principales del controlador formatos .
     *
     * @return void.
     */
	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Formatos();
		$this->namefilter = "parametersfilterformatos";
		$this->route = "/administracion/formatos";
		$this->namepages ="pages_formatos";
		$this->namepageactual ="page_actual_formatos";
		$this->_view->route = $this->route;
		if(Session::getInstance()->get($this->namepages)){
			$this->pages = Session::getInstance()->get($this->namepages);
		} else {
			$this->pages = 20;
		}
		parent::init();
	}


	/**
     * Recibe la informacion y  muestra un listado de  formatos con sus respectivos filtros.
     *
     * @return void.
     */
	public function indexAction()
	{
		$title = "Administración de Documentos";
		$this->getLayout()->setTitle($title);
		$this->_view->titlesection = $title;
		$this->filters();
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$filters = $this->getFilter();
		$order = "orden ASC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = $this->pages;
		$page = $this->_getSanitizedParam("page");
		if (!$page && Session::getInstance()->get($this->namepageactual)) {
		   	$page = Session::getInstance()->get($this->namepageactual);
		   	$start = ($page - 1) * $amount;
		} else if(!$page){
			$start = 0;
		   	$page=1;
			Session::getInstance()->set($this->namepageactual,$page);
		} else {
			Session::getInstance()->set($this->namepageactual,$page);
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->register_number = count($list);
		$this->_view->pages = $this->pages;
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->list_formato_seccion = $this->getFormatoseccion();
		$this->_view->list_formato_estado = $this->getFormatoestado();
	}

	/**
     * Genera la Informacion necesaria para editar o crear un  formatos  y muestra su formulario
     *
     * @return void.
     */
	public function manageAction()
	{
		$this->_view->route = $this->route;
		$this->_csrf_section = "manage_formatos_".date("YmdHis");
		$this->_csrf->generateCode($this->_csrf_section);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$this->_view->list_formato_seccion = $this->getFormatoseccion();
		$this->_view->list_formato_estado = $this->getFormatoestado();
		$this->_view->list_formato_subcategoria = $this->getFormatosubcategoria();

		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->formato_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$title = "Actualizar Documentos";
				$this->getLayout()->setTitle($title);
				$this->_view->titlesection = $title;
			}else{
				$this->_view->routeform = $this->route."/insert";
				$title = "Crear Documentos";
				$this->getLayout()->setTitle($title);
				$this->_view->titlesection = $title;
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$title = "Crear Documentos";
			$this->getLayout()->setTitle($title);
			$this->_view->titlesection = $title;
		}
	}

	/**
     * Inserta la informacion de un formatos  y redirecciona al listado de formatos.
     *
     * @return void.
     */
	public function insertAction(){
		$this->setLayout('blanco');
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf ) {	
			$data = $this->getData();
			$uploadDocument =  new Core_Model_Upload_Document();
			if($_FILES['formato_archivo']['name'] != ''){
				$data['formato_archivo'] = $uploadDocument->upload("formato_archivo");
			}
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
			$data['formato_id']= $id;
			$data['log_log'] = print_r($data,true);
			$data['log_tipo'] = 'CREAR FORMATOS';
			$logModel = new Administracion_Model_DbTable_Log();
			$logModel->insert($data);
		}
		header('Location: '.$this->route.''.'');
	}

	/**
     * Recibe un identificador  y Actualiza la informacion de un formatos  y redirecciona al listado de formatos.
     *
     * @return void.
     */
	public function updateAction(){
		$this->setLayout('blanco');
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf ) {
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->formato_id) {
				$data = $this->getData();
					$uploadDocument =  new Core_Model_Upload_Document();
				if($_FILES['formato_archivo']['name'] != ''){
					if($content->formato_archivo){
						$uploadDocument->delete($content->formato_archivo);
					}
					$data['formato_archivo'] = $uploadDocument->upload("formato_archivo");
				} else {
					$data['formato_archivo'] = $content->formato_archivo;
				}
				$this->mainModel->update($data,$id);
			}
			$data['formato_id']=$id;
			$data['log_log'] = print_r($data,true);
			$data['log_tipo'] = 'EDITAR FORMATOS';
			$logModel = new Administracion_Model_DbTable_Log();
			$logModel->insert($data);}
		header('Location: '.$this->route.''.'');
	}

	/**
     * Recibe un identificador  y elimina un formatos  y redirecciona al listado de formatos.
     *
     * @return void.
     */
	public function deleteAction()
	{
		$this->setLayout('blanco');
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_csrf_section] == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$uploadDocument =  new Core_Model_Upload_Document();
					if (isset($content->formato_archivo) && $content->formato_archivo != '') {
						$uploadDocument->delete($content->formato_archivo);
					}
					$this->mainModel->deleteRegister($id);$data = (array)$content;
					$data['log_log'] = print_r($data,true);
					$data['log_tipo'] = 'BORRAR FORMATOS';
					$logModel = new Administracion_Model_DbTable_Log();
					$logModel->insert($data); }
			}
		}
		header('Location: '.$this->route.''.'');
	}

	/**
     * Recibe la informacion del formulario y la retorna en forma de array para la edicion y creacion de Formatos.
     *
     * @return array con toda la informacion recibida del formulario.
     */
	private function getData()
	{
		$data = array();
		$data['formato_nombre'] = $this->_getSanitizedParam("formato_nombre");
		$data['formato_archivo'] = "";
		if($this->_getSanitizedParam("formato_seccion") == '' ) {
			$data['formato_seccion'] = '0';
		} else {
			$data['formato_seccion'] = $this->_getSanitizedParam("formato_seccion");
		}
		if($this->_getSanitizedParam("formato_subcategoria") == '' ) {
			$data['formato_subcategoria'] = '0';
		} else {
			$data['formato_subcategoria'] = $this->_getSanitizedParam("formato_subcategoria");
		}
		if($this->_getSanitizedParam("formato_estado") == '' ) {
			$data['formato_estado'] = '0';
		} else {
			$data['formato_estado'] = $this->_getSanitizedParam("formato_estado");
		}
		return $data;
	}

	/**
     * Genera los valores del campo Seccion.
     *
     * @return array cadena con los valores del campo Seccion.
     */
	private function getFormatoseccion()
	{
		$array = array();
		$array['1'] = 'Comunicados';
		$array['2'] = 'Resoluciones';
		return $array;
	}
	private function getFormatosubcategoria()
	{
		$array = array();
		$array['1'] = 'Liga';
		$array['2'] = 'Carreras';
		$array['3'] = 'Artístico';
		$array['4'] = 'Hockey Línea';
		$array['5'] = 'Hockey Patín';



		return $array;
	}


	/**
     * Genera los valores del campo Estado.
     *
     * @return array cadena con los valores del campo Estado.
     */
	private function getFormatoestado()
	{
		$array = array();
		$array['1'] = 'Activo';
		$array['2'] = 'Inactivo';
		return $array;
	}

	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {
    	$filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);
            if ($filters->formato_nombre != '') {
                $filtros = $filtros." AND formato_nombre LIKE '%".$filters->formato_nombre."%'";
            }
            if ($filters->formato_seccion != '') {
                $filtros = $filtros." AND formato_seccion ='".$filters->formato_seccion."'";
            }
            if ($filters->formato_estado != '') {
                $filtros = $filtros." AND formato_estado ='".$filters->formato_estado."'";
            }
		}
        return $filtros;
    }

    /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        if ($this->getRequest()->isPost()== true) {
        	Session::getInstance()->set($this->namepageactual,1);
            $parramsfilter = array();
					$parramsfilter['formato_nombre'] =  $this->_getSanitizedParam("formato_nombre");
					$parramsfilter['formato_seccion'] =  $this->_getSanitizedParam("formato_seccion");
					$parramsfilter['formato_estado'] =  $this->_getSanitizedParam("formato_estado");Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
            Session::getInstance()->set($this->namefilter, '');
            Session::getInstance()->set($this->namepageactual,1);
        }
    }
}