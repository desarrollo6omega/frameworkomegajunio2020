<?php
/**
* Controlador de Convenios que permite la  creacion, edicion  y eliminacion de los convenios del Sistema
*/
class Administracion_conveniosController extends Administracion_mainController
{
	/**
	 * $mainModel  instancia del modelo de  base de datos convenios
	 * @var modeloContenidos
	 */
	public $mainModel;

	/**
	 * $route  url del controlador base
	 * @var string
	 */
	protected $route;

	/**
	 * $pages cantidad de registros a mostrar por pagina]
	 * @var integer
	 */
	protected $pages ;

	/**
	 * $namefilter nombre de la variable a la fual se le van a guardar los filtros
	 * @var string
	 */
	protected $namefilter;

	/**
	 * $_csrf_section  nombre de la variable general csrf  que se va a almacenar en la session
	 * @var string
	 */
	protected $_csrf_section = "administracion_convenios";

	/**
	 * $namepages nombre de la pvariable en la cual se va a guardar  el numero de seccion en la paginacion del controlador
	 * @var string
	 */
	protected $namepages;



	/**
     * Inicializa las variables principales del controlador convenios .
     *
     * @return void.
     */
	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Convenios();
		$this->namefilter = "parametersfilterconvenios";
		$this->route = "/administracion/convenios";
		$this->namepages ="pages_convenios";
		$this->namepageactual ="page_actual_convenios";
		$this->_view->route = $this->route;
		if(Session::getInstance()->get($this->namepages)){
			$this->pages = Session::getInstance()->get($this->namepages);
		} else {
			$this->pages = 20;
		}
		parent::init();
	}


	/**
     * Recibe la informacion y  muestra un listado de  convenios con sus respectivos filtros.
     *
     * @return void.
     */
	public function indexAction()
	{
		$title = "Administración de Convenios";
		$this->getLayout()->setTitle($title);
		$this->_view->titlesection = $title;
		$this->filters();
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$filters = $this->getFilter();
		$order = "orden ASC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = $this->pages;
		$page = $this->_getSanitizedParam("page");
		if (!$page && Session::getInstance()->get($this->namepageactual)) {
		   	$page = Session::getInstance()->get($this->namepageactual);
		   	$start = ($page - 1) * $amount;
		} else if(!$page){
			$start = 0;
		   	$page=1;
			Session::getInstance()->set($this->namepageactual,$page);
		} else {
			Session::getInstance()->set($this->namepageactual,$page);
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->register_number = count($list);
		$this->_view->pages = $this->pages;
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->list_convenios_categoria = $this->getConvenioscategoria();
		$this->_view->list_convenios_estado = $this->getConveniosestado();
	}

	/**
     * Genera la Informacion necesaria para editar o crear un  convenios  y muestra su formulario
     *
     * @return void.
     */
	public function manageAction()
	{
		$this->_view->route = $this->route;
		$this->_csrf_section = "manage_convenios_".date("YmdHis");
		$this->_csrf->generateCode($this->_csrf_section);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$this->_view->list_convenios_categoria = $this->getConvenioscategoria();
		$this->_view->list_convenios_estado = $this->getConveniosestado();
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->convenios_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$title = "Actualizar Convenios";
				$this->getLayout()->setTitle($title);
				$this->_view->titlesection = $title;
			}else{
				$this->_view->routeform = $this->route."/insert";
				$title = "Crear Convenios";
				$this->getLayout()->setTitle($title);
				$this->_view->titlesection = $title;
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$title = "Crear Convenios";
			$this->getLayout()->setTitle($title);
			$this->_view->titlesection = $title;
		}
	}

	/**
     * Inserta la informacion de un convenios  y redirecciona al listado de convenios.
     *
     * @return void.
     */
	public function insertAction(){
		$this->setLayout('blanco');
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf ) {	
			$data = $this->getData();
			$uploadImage =  new Core_Model_Upload_Image();
			if($_FILES['convenios_imagen']['name'] != ''){
				$data['convenios_imagen'] = $uploadImage->upload("convenios_imagen");
			}
			$uploadDocument =  new Core_Model_Upload_Document();
			if($_FILES['convenios_archivo']['name'] != ''){
				$data['convenios_archivo'] = $uploadDocument->upload("convenios_archivo");
			}
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
			$data['convenios_id']= $id;
			$data['log_log'] = print_r($data,true);
			$data['log_tipo'] = 'CREAR CONVENIOS';
			$logModel = new Administracion_Model_DbTable_Log();
			$logModel->insert($data);
		}
		header('Location: '.$this->route.''.'');
	}

	/**
     * Recibe un identificador  y Actualiza la informacion de un convenios  y redirecciona al listado de convenios.
     *
     * @return void.
     */
	public function updateAction(){
		$this->setLayout('blanco');
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf ) {
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->convenios_id) {
				$data = $this->getData();
				$uploadImage =  new Core_Model_Upload_Image();
				if($_FILES['convenios_imagen']['name'] != ''){
					if($content->convenios_imagen){
						$uploadImage->delete($content->convenios_imagen);
					}
					$data['convenios_imagen'] = $uploadImage->upload("convenios_imagen");
				} else {
					$data['convenios_imagen'] = $content->convenios_imagen;
				}
				$uploadDocument =  new Core_Model_Upload_Document();
				if($_FILES['convenios_archivo']['name'] != ''){
					if($content->convenios_archivo){
						$uploadDocument->delete($content->convenios_archivo);
					}
					$data['convenios_archivo'] = $uploadDocument->upload("convenios_archivo");
				} else {
					$data['convenios_archivo'] = $content->convenios_archivo;
				}
				$this->mainModel->update($data,$id);
			}
			$data['convenios_id']=$id;
			$data['log_log'] = print_r($data,true);
			$data['log_tipo'] = 'EDITAR CONVENIOS';
			$logModel = new Administracion_Model_DbTable_Log();
			$logModel->insert($data);}
		header('Location: '.$this->route.''.'');
	}

	/**
     * Recibe un identificador  y elimina un convenios  y redirecciona al listado de convenios.
     *
     * @return void.
     */
	public function deleteAction()
	{
		$this->setLayout('blanco');
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_csrf_section] == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$uploadImage =  new Core_Model_Upload_Image();
					if (isset($content->convenios_imagen) && $content->convenios_imagen != '') {
						$uploadImage->delete($content->convenios_imagen);
					}
					$uploadDocument =  new Core_Model_Upload_Document();
					if (isset($content->convenios_archivo) && $content->convenios_archivo != '') {
						$uploadDocument->delete($content->convenios_archivo);
					}
					$this->mainModel->deleteRegister($id);$data = (array)$content;
					$data['log_log'] = print_r($data,true);
					$data['log_tipo'] = 'BORRAR CONVENIOS';
					$logModel = new Administracion_Model_DbTable_Log();
					$logModel->insert($data); }
			}
		}
		header('Location: '.$this->route.''.'');
	}

	/**
     * Recibe la informacion del formulario y la retorna en forma de array para la edicion y creacion de Convenios.
     *
     * @return array con toda la informacion recibida del formulario.
     */
	private function getData()
	{
		$data = array();
		$data['convenios_nombre'] = $this->_getSanitizedParam("convenios_nombre");
		$data['convenios_imagen'] = "";
		$data['convenios_descripcion'] = $this->_getSanitizedParamHtml("convenios_descripcion");
		if($this->_getSanitizedParam("convenios_categoria") == '' ) {
			$data['convenios_categoria'] = '0';
		} else {
			$data['convenios_categoria'] = $this->_getSanitizedParam("convenios_categoria");
		}
		$data['convenios_nombrearchivo'] = $this->_getSanitizedParam("convenios_nombrearchivo");
		$data['convenios_archivo'] = "";
		if($this->_getSanitizedParam("convenios_estado") == '' ) {
			$data['convenios_estado'] = '0';
		} else {
			$data['convenios_estado'] = $this->_getSanitizedParam("convenios_estado");
		}
		return $data;
	}

	/**
     * Genera los valores del campo Categoria.
     *
     * @return array cadena con los valores del campo Categoria.
     */
	private function getConvenioscategoria()
	{
		$modelData = new Administracion_Model_DbTable_Dependconvenioscategoria();
		$data = $modelData->getList();
		$array = array();
		foreach ($data as $key => $value) {
			$array[$value->convenios_categoria_id] = $value->convenios_categoria_nombre;
		}
		return $array;
	}


	/**
     * Genera los valores del campo Estado.
     *
     * @return array cadena con los valores del campo Estado.
     */
	private function getConveniosestado()
	{
		$array = array();
		$array['1'] = 'Activo';
		$array['2'] = 'Inactivo';
		return $array;
	}

	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {
    	$filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);
            if ($filters->convenios_nombre != '') {
                $filtros = $filtros." AND convenios_nombre LIKE '%".$filters->convenios_nombre."%'";
            }
            if ($filters->convenios_imagen != '') {
                $filtros = $filtros." AND convenios_imagen LIKE '%".$filters->convenios_imagen."%'";
            }
            if ($filters->convenios_categoria != '') {
                $filtros = $filtros." AND convenios_categoria LIKE '%".$filters->convenios_categoria."%'";
            }
            if ($filters->convenios_estado != '') {
                $filtros = $filtros." AND convenios_estado ='".$filters->convenios_estado."'";
            }
		}
        return $filtros;
    }

    /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        if ($this->getRequest()->isPost()== true) {
        	Session::getInstance()->set($this->namepageactual,1);
            $parramsfilter = array();
					$parramsfilter['convenios_nombre'] =  $this->_getSanitizedParam("convenios_nombre");
					$parramsfilter['convenios_imagen'] =  $this->_getSanitizedParam("convenios_imagen");
					$parramsfilter['convenios_categoria'] =  $this->_getSanitizedParam("convenios_categoria");
					$parramsfilter['convenios_estado'] =  $this->_getSanitizedParam("convenios_estado");Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
            Session::getInstance()->set($this->namefilter, '');
            Session::getInstance()->set($this->namepageactual,1);
        }
    }
}