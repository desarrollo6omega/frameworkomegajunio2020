<?php

/**
 * Controlador de Convenios que permite la  creacion, edicion  y eliminacion de los convenios del Sistema
 */
class Administracion_disciplinasController extends Administracion_mainController
{
	/**
	 * $mainModel  instancia del modelo de  base de datos convenios
	 * @var modeloContenidos
	 */
	public $mainModel;

	/**
	 * $route  url del controlador base
	 * @var string
	 */
	protected $route;

	/**
	 * $pages cantidad de registros a mostrar por pagina]
	 * @var integer
	 */
	protected $pages;

	/**
	 * $namefilter nombre de la variable a la fual se le van a guardar los filtros
	 * @var string
	 */
	protected $namefilter;

	/**
	 * $_csrf_section  nombre de la variable general csrf  que se va a almacenar en la session
	 * @var string
	 */
	protected $_csrf_section = "administracion_disciplinas";

	/**
	 * $namepages nombre de la pvariable en la cual se va a guardar  el numero de seccion en la paginacion del controlador
	 * @var string
	 */
	protected $namepages;



	/**
	 * Inicializa las variables principales del controlador convenios .
	 *
	 * @return void.
	 */
	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Disciplinas();
		$this->namefilter = "parametersfilterdisciplinas";
		$this->route = "/administracion/disciplinas";
		$this->namepages = "pages_disciplinas";
		$this->namepageactual = "page_actual_disciplinas";
		$this->_view->route = $this->route;
		if (Session::getInstance()->get($this->namepages)) {
			$this->pages = Session::getInstance()->get($this->namepages);
		} else {
			$this->pages = 20;
		}
		parent::init();
	}


	/**
	 * Recibe la informacion y  muestra un listado de  convenios con sus respectivos filtros.
	 *
	 * @return void.
	 */
	public function indexAction()
	{
		$title = "Administración de Disciplinas";
		$this->getLayout()->setTitle($title);
		$this->_view->titlesection = $title;
		$this->filters();
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$filters = (object) Session::getInstance()->get($this->namefilter);
		$this->_view->filters = $filters;
		$filters = $this->getFilter();
		$order = "orden ASC";
		$list = $this->mainModel->getList($filters, $order);
		$amount = $this->pages;
		$page = $this->_getSanitizedParam("page");
		if (!$page && Session::getInstance()->get($this->namepageactual)) {
			$page = Session::getInstance()->get($this->namepageactual);
			$start = ($page - 1) * $amount;
		} else if (!$page) {
			$start = 0;
			$page = 1;
			Session::getInstance()->set($this->namepageactual, $page);
		} else {
			Session::getInstance()->set($this->namepageactual, $page);
			$start = ($page - 1) * $amount;
		}
		$this->_view->register_number = count($list);
		$this->_view->pages = $this->pages;
		$this->_view->totalpages = ceil(count($list) / $amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters, $order, $start, $amount);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->list_disciplinas_categoria = $this->getDisciplinascategoria();
		$this->_view->list_disciplinas_estado = $this->getDisciplinasestado();
	}

	/**
	 * Genera la Informacion necesaria para editar o crear un  convenios  y muestra su formulario
	 *
	 * @return void.
	 */
	public function manageAction()
	{
		$this->_view->route = $this->route;
		$this->_csrf_section = "manage_disciplinas_" . date("YmdHis");
		$this->_csrf->generateCode($this->_csrf_section);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$this->_view->list_disciplinas_categoria = $this->getDisciplinascategoria();
		$this->_view->list_disciplinas_estado = $this->getDisciplinasestado();
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if ($content->disciplinas_id) {
				$this->_view->content = $content;
				$this->_view->routeform = $this->route . "/update";
				$title = "Actualizar Disciplinas";
				$this->getLayout()->setTitle($title);
				$this->_view->titlesection = $title;
			} else {
				$this->_view->routeform = $this->route . "/insert";
				$title = "Crear Disciplinas";
				$this->getLayout()->setTitle($title);
				$this->_view->titlesection = $title;
			}
		} else {
			$this->_view->routeform = $this->route . "/insert";
			$title = "Crear Disciplinas";
			$this->getLayout()->setTitle($title);
			$this->_view->titlesection = $title;
		}
	}

	/**
	 * Inserta la informacion de un convenios  y redirecciona al listado de convenios.
	 *
	 * @return void.
	 */
	public function insertAction()
	{
		$this->setLayout('blanco');
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf) {
			$data = $this->getData();
			$uploadImage =  new Core_Model_Upload_Image();

			if ($_FILES['disciplinas_imagen']['name'] != '') {
				$data['disciplinas_imagen'] = $uploadImage->upload("disciplinas_imagen");
			}

			$uploadDocument =  new Core_Model_Upload_Document();
			if ($_FILES['disciplinas_archivo']['name'] != '') {
				$data['disciplinas_archivo'] = $uploadDocument->upload("disciplinas_archivo");
			}

			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id, $id);

			$data['disciplinas_id'] = $id;
			$data['log_log'] = print_r($data, true);
			$data['log_tipo'] = 'CREAR Disciplinas';
			$logModel = new Administracion_Model_DbTable_Log();
			$logModel->insert($data);
		}
		header('Location: ' . $this->route . '' . '');
	}

	/**
	 * Recibe un identificador  y Actualiza la informacion de un convenios  y redirecciona al listado de convenios.
	 *
	 * @return void.
	 */
	public function updateAction()
	{
		$this->setLayout('blanco');
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf) {
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->disciplinas_id) {
				$data = $this->getData();
				$uploadImage =  new Core_Model_Upload_Image();
				if ($_FILES['disciplinas_imagen']['name'] != '') {
					if ($content->disciplinas_imagen) {
						$uploadImage->delete($content->disciplinas_imagen);
					}
					$data['disciplinas_imagen'] = $uploadImage->upload("disciplinas_imagen");
				} else {
					$data['disciplinas_imagen'] = $content->disciplinas_imagen;
				}
				$uploadDocument =  new Core_Model_Upload_Document();
				if ($_FILES['disciplinas_archivo']['name'] != '') {
					if ($content->disciplinas_archivo) {
						$uploadDocument->delete($content->disciplinas_archivo);
					}
					$data['disciplinas_archivo'] = $uploadDocument->upload("disciplinas_archivo");
				} else {
					$data['disciplinas_archivo'] = $content->disciplinas_archivo;
				}
				$this->mainModel->update($data, $id);
			}
			$data['disciplinas_id'] = $id;
			$data['log_log'] = print_r($data, true);
			$data['log_tipo'] = 'EDITAR Disciplinas';
			$logModel = new Administracion_Model_DbTable_Log();
			$logModel->insert($data);
		}
		header('Location: ' . $this->route . '' . '');
	}

	/**
	 * Recibe un identificador  y elimina un convenios  y redirecciona al listado de convenios.
	 *
	 * @return void.
	 */
	public function deleteAction()
	{
		$this->setLayout('blanco');
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_csrf_section] == $csrf) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$uploadImage =  new Core_Model_Upload_Image();
					if (isset($content->disciplinas_imagen) && $content->disciplinas_imagen != '') {
						$uploadImage->delete($content->disciplinas_imagen);
					}
					$uploadDocument =  new Core_Model_Upload_Document();
					if (isset($content->disciplinas_archivo) && $content->disciplinas_archivo != '') {
						$uploadDocument->delete($content->disciplinas_archivo);
					}
					$this->mainModel->deleteRegister($id);
					$data = (array) $content;
					$data['log_log'] = print_r($data, true);
					$data['log_tipo'] = 'BORRAR Disciplinas';
					$logModel = new Administracion_Model_DbTable_Log();
					$logModel->insert($data);
				}
			}
		}
		header('Location: ' . $this->route . '' . '');
	}

	/**
	 * Recibe la informacion del formulario y la retorna en forma de array para la edicion y creacion de Convenios.
	 *
	 * @return array con toda la informacion recibida del formulario.
	 */
	private function getData()
	{
		$data = array();
		$data['disciplinas_nombre'] = $this->_getSanitizedParam("disciplinas_nombre");
		$data['disciplinas_imagen'] = "";
		if ($this->_getSanitizedParam("disciplinas_estado") == '') {
			$data['disciplinas_estado'] = '0';
		} else {
			$data['disciplinas_estado'] = $this->_getSanitizedParam("disciplinas_estado");
		}
		$data['disciplinas_descripcion'] = $this->_getSanitizedParamHtml("disciplinas_descripcion");
		if ($this->_getSanitizedParam("disciplinas_categoria") == '') {
			$data['disciplinas_categoria'] = '0';
		} else {
			$data['disciplinas_categoria'] = $this->_getSanitizedParam("disciplinas_categoria");
		}
		$data['disciplinas_nombrearchivo'] = $this->_getSanitizedParam("disciplinas_nombrearchivo");
		$data['disciplinas_archivo'] = "";

		return $data;
	}

	/**
	 * Genera los valores del campo Categoria.
	 *
	 * @return array cadena con los valores del campo Categoria.
	 */
	private function getDisciplinascategoria()
	{
		$modelData = new Administracion_Model_DbTable_Dependdisciplinascategoria();
		$data = $modelData->getList();
		$array = array();
		foreach ($data as $key => $value) {
			$array[$value->disciplinas_categoria_id] = $value->disciplinas_categoria_nombre;
		}
		return $array;
	}


	/**
	 * Genera los valores del campo Estado.
	 *
	 * @return array cadena con los valores del campo Estado.
	 */
	private function getDisciplinasestado()
	{
		$array = array();
		$array['1'] = 'Activo';
		$array['2'] = 'Inactivo';
		return $array;
	}

	/**
	 * Genera la consulta con los filtros de este controlador.
	 *
	 * @return array cadena con los filtros que se van a asignar a la base de datos
	 */
	protected function getFilter()
	{
		$filtros = " 1 = 1 ";
		if (Session::getInstance()->get($this->namefilter) != "") {
			$filters = (object) Session::getInstance()->get($this->namefilter);
			if ($filters->disciplinas_nombre != '') {
				$filtros = $filtros . " AND disciplinas_nombre LIKE '%" . $filters->disciplinas_nombre . "%'";
			}
			if ($filters->disciplinas_imagen != '') {
				$filtros = $filtros . " AND disciplinas_imagen LIKE '%" . $filters->disciplinas_imagen . "%'";
			}
			if ($filters->disciplinas_categoria != '') {
				$filtros = $filtros . " AND disciplinas_categoria LIKE '%" . $filters->disciplinas_categoria . "%'";
			}
			if ($filters->disciplinas_estado != '') {
				$filtros = $filtros . " AND disciplinas_estado ='" . $filters->disciplinas_estado . "'";
			}
		}
		return $filtros;
	}

	/**
	 * Recibe y asigna los filtros de este controlador
	 *
	 * @return void
	 */
	protected function filters()
	{
		if ($this->getRequest()->isPost() == true) {
			Session::getInstance()->set($this->namepageactual, 1);
			$parramsfilter = array();
			$parramsfilter['disciplinas_nombre'] =  $this->_getSanitizedParam("disciplinas_nombre");
			$parramsfilter['disciplinas_imagen'] =  $this->_getSanitizedParam("disciplinas_imagen");
			$parramsfilter['disciplinas_categoria'] =  $this->_getSanitizedParam("disciplinas_categoria");
			$parramsfilter['disciplinas_estado'] =  $this->_getSanitizedParam("disciplinas_estado");
			Session::getInstance()->set($this->namefilter, $parramsfilter);
		}
		if ($this->_getSanitizedParam("cleanfilter") == 1) {
			Session::getInstance()->set($this->namefilter, '');
			Session::getInstance()->set($this->namepageactual, 1);
		}
	}
}
