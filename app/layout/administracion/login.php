<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title><?= $this->_titlepage ?></title>
    <?php $infopageModel = new Page_Model_DbTable_Informacion();
    $infopage = $infopageModel->getById(1);
    ?>

    <link rel="stylesheet" href="/components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/components/Font-Awesome/web-fonts-with-css/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/skins/administracion/css/global.css">
    <link rel="shortcut icon" href="/images/<?= $infopage->info_pagina_favicon; ?>">

</head>

<body class="login-fondo">
    <div class="login-caja">
        <h1><?= $this->_titlepage ?></h1>
        <div class="login-logo"><img src="/images/logoadmin.png"></div>
        <div class="login-content"><?= $this->_content ?></div>
    </div>

    <div class="login-derechos">&copy;2018 Todos los derechos reservados | Diseñado por WHY CREATIVE SOLUTIONS
    </div>

    <script src="/components/jquery/dist/jquery.min.js"></script>
    <script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="/skins/administracion/js/main.js"></script>
</body>

</html>